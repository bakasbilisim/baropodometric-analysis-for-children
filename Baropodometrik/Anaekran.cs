﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Globalization;

namespace Baropodometrik
{
    public partial class Anaekran : Form
    {
        public Anaekran()
        {
            InitializeComponent();
        }
        private static Byte[,] solAyakDizi = new Byte[64, 32];
        private static Byte[,] solAyakDiziEski = new Byte[64, 32];
        private static Byte[,] sagAyakDizi = new Byte[64, 32];
        private static Byte[,] sagAyakDiziEski = new Byte[64, 32];
        private static Byte[,] maskDizi = new Byte[64, 32];
        private static String dosyaYolu = "";
        private static String dosyaAdi = "";
        private static String seciliAyak = "";
        private static String hastaKodu = "";
        private static String hastaTaniGrubu = "";
        private static String hastaTaniSeviyesi = "";
        private static String hastaTaniGrubuSol = "";
        private static String hastaTaniSeviyesiSol = "";
        private static String hastaTaniGrubuSag = "";
        private static String hastaTaniSeviyesiSag = "";
        private static String tanimlananhastaTaniGrubu = "";
        private static String tanimlananhastaTaniSeviyesi = "";
        private static String tanimlananhastaTaniGrubuSol = "";
        private static String tanimlananhastaTaniSeviyesiSol = "";
        private static String tanimlananhastaTaniGrubuSag = "";
        private static String tanimlananhastaTaniSeviyesiSag = "";
        private static String scale = "";
        private static String filtreler = "";
        private static Boolean dosyaSecili = false;
        private static Boolean isMouseDown = false;
        private static Point lastPoint;
        private static Boolean degisiklikYapildi = false;
        private static Point degisenPixel;
        private static Byte seciliRenk;
        private static Bitmap deneme;
        private static System.Timers.Timer timer, timer2;
        private static int cikarmaKatsayisi = 0;
        private static int pixelSayaci = 0;
        private static String scaleRandom = "";
        private static Boolean lpgYapildi = false;
        private static bool kaydet = true;
        private static int dosyaVarSayac = 0;
        private static bool timerflag = false;
        private static bool timer2flag = false;
        //Uygulama açıldığında çalışır
        private void Form1_Load(object sender, EventArgs e)
        {
            cb_alan.SelectedIndex = 0;
            cb_lpg.SelectedIndex = 0;
            cb_lpm.SelectedIndex = 0;
            cb_decisiontree.SelectedIndex = 0;
            cb_randomforest.SelectedIndex = 0;
            cb_linearregression.SelectedIndex = 0;
            cb_batch1.SelectedIndex = 0;
            cb_batch2.SelectedIndex = 0;
            pictureBox1.MouseDown += new MouseEventHandler(pictureBox1_MouseDown);
            pictureBox1.MouseMove += new MouseEventHandler(pictureBox1_MouseMove);
            pictureBox1.MouseUp += new MouseEventHandler(pictureBox1_MouseUp);
            pictureBox2.MouseDown += new MouseEventHandler(pictureBox2_MouseDown);
            pictureBox2.MouseMove += new MouseEventHandler(pictureBox2_MouseMove);
            pictureBox2.MouseUp += new MouseEventHandler(pictureBox2_MouseUp);
            timer = new System.Timers.Timer();
            timer.Interval = 50;
            timer.Elapsed += OnTimedEvent;
            timer.AutoReset = true;
            timer2 = new System.Timers.Timer();
            timer2.Interval = 50;
            timer2.Elapsed += OnTimedEvent2;
            timer2.AutoReset = true;
            
        }
        //Yeni bir scale değeri üretir
        private void newscale()
        {
            Random rand = new Random();
            scaleRandom = Convert.ToString(Math.Round(rand.NextDouble() * (2.00 - 1.00) + 1.00, 2));
            if (scaleRandom.Length == 4)
            {
                scaleRandom = scaleRandom.Substring(0, 1) + "." + scaleRandom.Substring(2, 2);
            }
            else if (scaleRandom.Length == 3)
            {
                scaleRandom = scaleRandom.Substring(0, 1) + "." + scaleRandom.Substring(2, 1);

            }
        }
        //Sol ayağı sürekli olarak günceller
        private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            pictureBox1guncelle();
        }
        //Sağ ayağı sürekli olarak günceller
        private void OnTimedEvent2(Object source, System.Timers.ElapsedEventArgs e)
        {
            pictureBox2guncelle();
        }
        //Sol ve sağ ayak için diziden dosyaya komutunu çalıştırır
        private void btn_dosyakaydet_Click(object sender, EventArgs e)
        {
            if (dosyaSecili)
            {
                if (MessageBox.Show("Değişiklikleri Bu Dosyanın Üzerine Kaydetmek İstiyor Musunuz?", "Dikkat", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    dizidenDosyaya(solAyakDizi, "sol");
                    dizidenDosyaya(sagAyakDizi, "sag");
                    MessageBox.Show("Dosya Kaydedildi");
                }
            }
            else
            {
                MessageBox.Show("Herhangi Bir Dosya Açık Değil", "Hata");
            }
        }
        //Sağ ve sol ayak için dizidenYeniDosyaya komutunu çalıştırır
        private void btn_yenidosyakaydet_Click(object sender, EventArgs e)
        {
            newscale();
            if (dosyaSecili)
            {
                if (dizidenYeniDosyaya(solAyakDizi, "sol") && dizidenYeniDosyaya(sagAyakDizi, "sag"))
                {
                    MessageBox.Show("Yeni Dosya Kaydedildi");
                }
                kaydet = true;
                dosyaVarSayac = 0;
            }
            else
            {
                MessageBox.Show("Herhangi Bir Dosya Açık Değil", "Hata");
            }
        }
        //File dialoglu yeni dosya kaydetme fonksiyonu
        private void dizidenYeniDosyaya2(Byte[,] dizi, String ayak)
        {
            var saveFile = new SaveFileDialog();
            saveFile.RestoreDirectory = true;
            saveFile.Title = "Dosya Kaydet";
            saveFile.Filter = "CSV Dosyası |*.csv";
            saveFile.DefaultExt = "csv";
            if (ayak == "sag")
            {
                saveFile.FileName = "BB-" + hastaKoduOlustur() + "-R.csv";
            }
            if (ayak == "sol")
            {
                saveFile.FileName = "BB-" + hastaKoduOlustur() + "-L.csv";
                dizi = getMirror(dizi);
            }
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                using (var writer = new StreamWriter(saveFile.FileName, false))
                {
                    if (ayak == "sag")
                    {
                        writer.WriteLine("BAKAS BILISIM");
                        writer.WriteLine("BAROPODOMETRIK VERI DOSYASI v1.2");
                        writer.WriteLine("SATIR SAYISI: 64");
                        writer.WriteLine("KOLON SAYISI: 32");
                        writer.WriteLine("DERINLIK: 8");
                        writer.WriteLine(hastaKoduOlustur());
                        writer.WriteLine(hastaTaniGrubuSag);
                        writer.WriteLine(hastaTaniSeviyesiSag);
                        writer.WriteLine("S/S: SAG");
                        writer.WriteLine("SCALE: "+scaleRandom);
                        writer.WriteLine("FILTRELER: DESPECKLE");
                        writer.WriteLine(tanimlananhastaTaniGrubuSag);
                        writer.WriteLine(tanimlananhastaTaniSeviyesiSag);
                        for (int i = 0; i < dizi.GetLength(0); i++)
                        {
                            for (int j = 0; j < dizi.GetLength(1) - 1; j++)
                            {
                                writer.Write(dizi[i, j] + ",");
                            }
                            writer.Write(dizi[i, dizi.GetLength(1) - 1]);
                            writer.WriteLine();
                        }
                    }
                    if (ayak == "sol")
                    {
                        writer.WriteLine("BAKAS BILISIM");
                        writer.WriteLine("BAROPODOMETRIK VERI DOSYASI v1.2");
                        writer.WriteLine("SATIR SAYISI: 64");
                        writer.WriteLine("KOLON SAYISI: 32");
                        writer.WriteLine("DERINLIK: 8");
                        writer.WriteLine(hastaKoduOlustur());
                        writer.WriteLine(hastaTaniGrubuSol);
                        writer.WriteLine(hastaTaniSeviyesiSol);
                        writer.WriteLine("S/S: SOL");
                        writer.WriteLine("SCALE: " + scaleRandom);
                        writer.WriteLine("FILTRELER: DESPECKLE");
                        writer.WriteLine(tanimlananhastaTaniGrubuSag);
                        writer.WriteLine(tanimlananhastaTaniSeviyesiSag);
                        for (int i = 0; i < dizi.GetLength(0); i++)
                        {
                            for (int j = 0; j < dizi.GetLength(1) - 1; j++)
                            {
                                writer.Write(dizi[i, j] + ",");
                            }
                            writer.Write(dizi[i, dizi.GetLength(1) - 1]);
                            writer.WriteLine();
                        }
                    }
                }
            }
        }
        //File dialogsuz yeni dosya kaydetme fonksiyonu
        private bool dizidenYeniDosyaya(Byte[,] dizi, String ayak)
        {
            dosyaYolu = dosyaYolu.Substring(0, dosyaYolu.Length - 15);
            if (ayak == "sag")
            {
                dosyaYolu += "BB-" + hastaKoduOlustur() + "-R.csv";
            }
            if (ayak == "sol")
            {
                dosyaYolu += "BB-" + hastaKoduOlustur() + "-L.csv";
                dizi = getMirror(dizi);
            }
            if (dosyaVarSayac == 0)
            {
                if (File.Exists(dosyaYolu))
                {
                    if (MessageBox.Show("Bu İsimde Dosyalar Var! Kayıt Etmek İstediğinize Emin Misiniz?", "Dikkat", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        kaydet = false;
                        return false;
                    }
                    dosyaVarSayac++;
                }
            }
            if (kaydet) 
            { 
                using (var writer = new StreamWriter(dosyaYolu, false))
                {
                    if (ayak == "sag")
                    {
                        writer.WriteLine("BAKAS BILISIM");
                        writer.WriteLine("BAROPODOMETRIK VERI DOSYASI v1.2");
                        writer.WriteLine("SATIR SAYISI: 64");
                        writer.WriteLine("KOLON SAYISI: 32");
                        writer.WriteLine("DERINLIK: 8");
                        writer.WriteLine(hastaKodu.Substring(0, hastaKodu.Length - 6) + hastaKoduOlustur());
                        writer.WriteLine(hastaTaniGrubuSag);
                        writer.WriteLine(hastaTaniSeviyesiSag);
                        writer.WriteLine("S/S: SAG");
                        writer.WriteLine("SCALE: " + scaleRandom);
                        writer.WriteLine("FILTRELER: DESPECKLE");
                        writer.WriteLine(tanimlananhastaTaniGrubuSag);
                        writer.WriteLine(tanimlananhastaTaniSeviyesiSag);
                        for (int i = 0; i < dizi.GetLength(0); i++)
                        {
                            for (int j = 0; j < dizi.GetLength(1) - 1; j++)
                            {
                                writer.Write(dizi[i, j] + ",");
                            }
                            writer.Write(dizi[i, dizi.GetLength(1) - 1]);
                            writer.WriteLine();
                        }
                    }
                    if (ayak == "sol")
                    {
                        writer.WriteLine("BAKAS BILISIM");
                        writer.WriteLine("BAROPODOMETRIK VERI DOSYASI v1.2");
                        writer.WriteLine("SATIR SAYISI: 64");
                        writer.WriteLine("KOLON SAYISI: 32");
                        writer.WriteLine("DERINLIK: 8");
                        writer.WriteLine(hastaKodu.Substring(0, hastaKodu.Length - 6) + hastaKoduOlustur());
                        writer.WriteLine(hastaTaniGrubuSol);
                        writer.WriteLine(hastaTaniSeviyesiSol);
                        writer.WriteLine("S/S: SOL");
                        writer.WriteLine("SCALE: " + scaleRandom);
                        writer.WriteLine("FILTRELER: DESPECKLE");
                        writer.WriteLine(tanimlananhastaTaniGrubuSol);
                        writer.WriteLine(tanimlananhastaTaniSeviyesiSol);
                        for (int i = 0; i < dizi.GetLength(0); i++)
                        {
                            for (int j = 0; j < dizi.GetLength(1) - 1; j++)
                            {
                                writer.Write(dizi[i, j] + ",");
                            }
                            writer.Write(dizi[i, dizi.GetLength(1) - 1]);
                            writer.WriteLine();
                        }
                    }
                }
                
            }
            return true;

        }
        //Açık olan dosya üzerine yapılan değişiklikleri kaydetme fonksiyonu
        private void dizidenDosyaya(Byte[,] dizi, String ayak)
        {
            dosyaYolu = dosyaYolu.Substring(0, dosyaYolu.Length - 15);
            if (ayak == "sag")
            {
                dosyaYolu += "BB-" + hastaKodu.Substring(hastaKodu.Length - 6, 6) + "-R.csv";
            }
            if (ayak == "sol")
            {
                dosyaYolu += "BB-" + hastaKodu.Substring(hastaKodu.Length - 6, 6) + "-L.csv";
                dizi = getMirror(dizi);
            }

            using (var writer = new StreamWriter(dosyaYolu, false))
            {
                if (ayak == "sag")
                {
                    writer.WriteLine("BAKAS BILISIM");
                    writer.WriteLine("BAROPODOMETRIK VERI DOSYASI v1.2");
                    writer.WriteLine("SATIR SAYISI: 64");
                    writer.WriteLine("KOLON SAYISI: 32");
                    writer.WriteLine("DERINLIK: 8");
                    writer.WriteLine(hastaKodu);
                    writer.WriteLine(hastaTaniGrubuSag);
                    writer.WriteLine(hastaTaniSeviyesiSag);
                    writer.WriteLine("S/S: SAG");
                    writer.WriteLine("SCALE: 1.24");
                    writer.WriteLine("FILTRELER: DESPECKLE");
                    writer.WriteLine(tanimlananhastaTaniGrubuSag);
                    writer.WriteLine(tanimlananhastaTaniSeviyesiSag);
                    for (int i = 0; i < dizi.GetLength(0); i++)
                    {
                        for (int j = 0; j < dizi.GetLength(1) - 1; j++)
                        {
                            writer.Write(dizi[i, j] + ",");
                        }
                        writer.Write(dizi[i, dizi.GetLength(1) - 1]);
                        writer.WriteLine();
                    }
                }
                if (ayak == "sol")
                {
                    writer.WriteLine("BAKAS BILISIM");
                    writer.WriteLine("BAROPODOMETRIK VERI DOSYASI v1.2");
                    writer.WriteLine("SATIR SAYISI: 64");
                    writer.WriteLine("KOLON SAYISI: 32");
                    writer.WriteLine("DERINLIK: 8");
                    writer.WriteLine(hastaKodu);
                    writer.WriteLine(hastaTaniGrubuSol);
                    writer.WriteLine(hastaTaniSeviyesiSol);
                    writer.WriteLine("S/S: SOL");
                    writer.WriteLine("SCALE: 1.24");
                    writer.WriteLine("FILTRELER: DESPECKLE");
                    writer.WriteLine(tanimlananhastaTaniGrubuSol);
                    writer.WriteLine(tanimlananhastaTaniSeviyesiSol);
                    for (int i = 0; i < dizi.GetLength(0); i++)
                    {
                        for (int j = 0; j < dizi.GetLength(1) - 1; j++)
                        {
                            writer.Write(dizi[i, j] + ",");
                        }
                        writer.Write(dizi[i, dizi.GetLength(1) - 1]);
                        writer.WriteLine();
                    }
                }
            }

        }
        //6 basamaklı hasta kodunu oluşturur
        private String hastaKoduOlustur()
        {
            int kod = int.Parse(hastaKodu.Substring(hastaKodu.Length - 6, 6));
            kod++;
            int basamak = 1;
            int x = kod;
            while (x > 9)
            {
                basamak++;
                x = x / 10;
            }
            String sonuc = "";
            for (int i = 0; i < 6 - basamak; i++)
            {
                sonuc += "0";
            }
            sonuc += Convert.ToString(kod);
            return sonuc;

        }
        //Dosya aç butonuna basıldığında çağırılır
        private void btn_dosyaac_Click(object sender, EventArgs e)
        {

            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "CSV Dosyası |*.csv";
            file.RestoreDirectory = true;
            file.CheckFileExists = true;
            file.Title = "CSV Dosyası Seçiniz..";

            if (file.ShowDialog() == DialogResult.OK)
            {
                string DosyaYolu = file.FileName;
                dosyaYolu = file.FileName;
                string DosyaAdi = file.SafeFileName;
                dosyaAdi = file.SafeFileName;
                if (DosyaAdi.Contains("-L"))
                {
                    solAyakDizi = DosyaAl(DosyaYolu);
                    solAyakDiziEski = solAyakDizi;
                    pictureBox1.Image = DizidenBitmape(buyutucu(solAyakDizi));
                    DosyaYolu = DosyaYolu.Substring(0, DosyaYolu.Length - 5);
                    DosyaYolu = DosyaYolu + "R.csv";
                    sagAyakDizi = DosyaAl(DosyaYolu);
                    sagAyakDiziEski = sagAyakDizi;
                    pictureBox2.Image = DizidenBitmape(buyutucu(sagAyakDizi));
                    lbl_filtrelertext.Text = filtreler.Substring(10);
                    lbl_scaletext.Text = scale.Substring(6);
                    lbl_solayaktaniseviyesitext.Text =  hastaTaniSeviyesiSol.Substring(21, 1);
                    lbl_sagayaktaniseviyesitext.Text = hastaTaniSeviyesiSag.Substring(21, 1);
                    lbl_solayaktanisitext.Text =  hastaTaniGrubuSol.Substring(18, hastaTaniGrubuSol.Length - 18);
                    lbl_sagayaktanisitext.Text =  hastaTaniGrubuSag.Substring(18, hastaTaniGrubuSag.Length - 18);
                    lbl_soltanimseviyesi.Text =  tanimlananhastaTaniSeviyesiSol.Substring(32, 1);
                    lbl_sagtanimseviyesi.Text =  tanimlananhastaTaniSeviyesiSag.Substring(32, 1);
                    lbl_soltanimgrubu.Text =  tanimlananhastaTaniGrubuSol.Substring(29, tanimlananhastaTaniGrubuSol.Length - 29);
                    lbl_sagtanimgrubu.Text =  tanimlananhastaTaniGrubuSag.Substring(29, tanimlananhastaTaniGrubuSag.Length - 29);
                    lbl_hastakodu.Text = hastaKodu.Substring(11);
                    dosyaSecili = true;
                    DosyaYolu = DosyaYolu.Substring(0, DosyaYolu.Length - 15);
                    DosyaYolu = DosyaYolu + "mask.csv";
                    maskDizi = maskAl(DosyaYolu);
                    maskSagAyak();
                    maskSolAyak();
                    pictureBox1guncelle();
                    pictureBox2guncelle();
                }
                else if (DosyaAdi.Contains("-R"))
                {
                    sagAyakDizi = DosyaAl(DosyaYolu);
                    sagAyakDiziEski = sagAyakDizi;
                    pictureBox2.Image = DizidenBitmape(buyutucu(sagAyakDizi));
                    DosyaYolu = DosyaYolu.Substring(0, DosyaYolu.Length - 5);
                    DosyaYolu = DosyaYolu + "L.csv";
                    solAyakDizi = DosyaAl(DosyaYolu);
                    solAyakDiziEski = solAyakDizi;
                    pictureBox1.Image = DizidenBitmape(buyutucu(solAyakDizi));
                    lbl_filtrelertext.Text = filtreler.Substring(10);
                    lbl_scaletext.Text = scale.Substring(6);
                    lbl_solayaktaniseviyesitext.Text = hastaTaniSeviyesiSol.Substring(21, 1);
                    lbl_sagayaktaniseviyesitext.Text = hastaTaniSeviyesiSag.Substring(21, 1);
                    lbl_solayaktanisitext.Text = hastaTaniGrubuSol.Substring(18, hastaTaniGrubuSol.Length - 18);
                    lbl_sagayaktanisitext.Text =hastaTaniGrubuSag.Substring(18, hastaTaniGrubuSag.Length - 18);
                    lbl_soltanimseviyesi.Text = tanimlananhastaTaniSeviyesiSol.Substring(32, 1);
                    lbl_sagtanimseviyesi.Text =  tanimlananhastaTaniSeviyesiSag.Substring(32, 1);
                    lbl_soltanimgrubu.Text =  tanimlananhastaTaniGrubuSol.Substring(29, tanimlananhastaTaniGrubuSol.Length - 29);
                    lbl_sagtanimgrubu.Text =  tanimlananhastaTaniGrubuSag.Substring(29, tanimlananhastaTaniGrubuSag.Length - 29);
                    lbl_hastakodu.Text = hastaKodu.Substring(11);
                    dosyaSecili = true;
                    DosyaYolu = DosyaYolu.Substring(0, DosyaYolu.Length - 15);
                    DosyaYolu = DosyaYolu + "mask.csv";
                    maskDizi = maskAl(DosyaYolu);
                    maskSolAyak();
                    maskSagAyak();
                    pictureBox1guncelle();
                    pictureBox2guncelle();
                }
            }

        }
        //Dizi olarak tutulan değerleri bitmap cinsine dünüştürür
        public static Bitmap DizidenBitmape(Byte[,] data)
        {

            Bitmap bmp = new Bitmap(data.GetLength(1), data.GetLength(0));

            for (int r = 0; r < data.GetLength(0); r++)
            {
                for (int c = 0; c < data.GetLength(1); c++)
                {
                    Byte value = data[r, c];
                    bmp.SetPixel(c, r, Color.FromArgb(value, value, value));
                }
            }

            return bmp;
        }
        //Dizideki her değeri 8 kat büyütür
        private Byte[,] buyutucu(Byte[,] array)
        {
            int buyumeMiktari = 8;
            Byte[,] sonarray = new Byte[array.GetLength(0) * buyumeMiktari, array.GetLength(1) * buyumeMiktari];
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    for (int k = i * buyumeMiktari; k < i * buyumeMiktari + buyumeMiktari; k++)
                    {
                        for (int m = j * buyumeMiktari; m < j * buyumeMiktari + buyumeMiktari; m++)
                        {
                            sonarray[k, m] = array[i, j];
                        }
                    }
                }
            }
            return sonarray;
        }
        //Dizideki her değeri büyüme miktarı kadar büyütür
        private Byte[,] buyutucu(Byte[,] array, int buyumeMiktari)
        {
            Byte[,] sonarray = new Byte[array.GetLength(0) * buyumeMiktari, array.GetLength(1) * buyumeMiktari];
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    for (int k = i * buyumeMiktari; k < i * buyumeMiktari + buyumeMiktari; k++)
                    {
                        for (int m = j * buyumeMiktari; m < j * buyumeMiktari + buyumeMiktari; m++)
                        {
                            sonarray[k, m] = array[i, j];
                        }
                    }
                }
            }
            return sonarray;
        }
        //Dizinin dikeydeki ayna görüntüsünü elde eder
        private static Byte[,] getMirror(Byte[,] dizi)
        {
            Byte[,] sondizi = new Byte[dizi.GetLength(0), dizi.GetLength(1)];
            for (int i = 0; i < dizi.GetLength(0); i++)
            {
                for (int j = 0; j < dizi.GetLength(1); j++)
                {
                    sondizi[i, j] = dizi[i, dizi.GetLength(1) - 1 - j];
                }
            }
            return sondizi;
        }
        //Belirtilen dosya yolundan dosyayı ve diğer eş dosyayı alır, okuyarak gerekli değişkenlere atar
        static Byte[,] DosyaAl(String Dosyayolu)
        {
            Byte[,] array = new Byte[64, 32];
            try
            {
                using (var reader = new StreamReader(@Dosyayolu))
                {
                    int satir = 1;
                    String ayak = "";
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(',');
                        if (satir == 9 && values[0] == "S/S: SOL")
                        {
                            ayak = "sol";
                        }
                        else if (satir == 9 && values[0] == "S/S: SAG")
                        {
                            ayak = "sag";
                        }
                        if (satir == 6)
                        {
                            hastaKodu = values[0];
                        }
                        if (satir == 7)
                        {
                            hastaTaniGrubu = values[0];
                        }
                        if (satir == 8)
                        {
                            hastaTaniSeviyesi = values[0];
                        }
                        if (satir == 10)
                        {
                            scale = values[0];
                        }
                        if (satir == 11)
                        {
                            filtreler = values[0];
                        }
                        if (satir == 12)
                        {
                            tanimlananhastaTaniGrubu = values[0];
                        }
                        if (satir == 13)
                        {
                            tanimlananhastaTaniSeviyesi = values[0];
                        }
                        if (satir > 13)
                        {
                            for (int sutun = 0; sutun < 32; sutun++)
                            {
                                array[satir - 14, sutun] = Byte.Parse(values[sutun]);
                            }
                        }
                        satir++;
                    }
                    if (ayak == "sag")
                    {
                        hastaTaniGrubuSag = hastaTaniGrubu;
                        hastaTaniSeviyesiSag = hastaTaniSeviyesi;
                        tanimlananhastaTaniGrubuSag = tanimlananhastaTaniGrubu;
                        tanimlananhastaTaniSeviyesiSag = tanimlananhastaTaniSeviyesi;
                        return array;
                    }
                    else
                    {
                        hastaTaniGrubuSol = hastaTaniGrubu;
                        hastaTaniSeviyesiSol = hastaTaniSeviyesi;
                        tanimlananhastaTaniGrubuSol = tanimlananhastaTaniGrubu;
                        tanimlananhastaTaniSeviyesiSol = tanimlananhastaTaniSeviyesi;
                        return getMirror(array);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Hata");
                return array;
            }

        }
        //Dosya yolundan maskeleme dosyasını okur
        static Byte[,] maskAl(String Dosyayolu)
        {
            Byte[,] array = new Byte[64, 32];
            try
            {
                using (var reader = new StreamReader(@Dosyayolu))
                {
                    int satir = 1;
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(',');

                        for (int sutun = 0; sutun < 32; sutun++)
                        {
                            array[satir - 1, sutun] = Byte.Parse(values[sutun]);
                        }                        
                        satir++;
                    }
                    
                }
                return array;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Hata");
                return array;
            }

        }
        //Sağ ayak dizisini maskeye uygun olarak filtreler
        private void maskSagAyak()
        {
            for(int i = 0; i < 64; i++)
            {
                for (int j = 0; j < 32; j++)
                {
                    if (maskDizi[i,j] == 0)
                    {
                        sagAyakDizi[i, j] = 0;
                    }
                }
            }
        }
        //Sol ayak dizisini maskeye uygun olarak filtreler
        private void maskSolAyak()
        {
            for (int i = 0; i < 64; i++)
            {
                for (int j = 0; j < 32; j++)
                {
                    if (maskDizi[i, j] == 0)
                    {
                        solAyakDizi[i, 31-j] = 0;
                    }
                }
            }
        }
        //Seçili ayağı yan tarafa ayna görüntüsü alarak kopyalar
        private void btn_mirror_Click(object sender, EventArgs e)
        {
            if (seciliAyak == "sag")
            {
                solAyakDiziEski = solAyakDizi;
                sagAyakDiziEski = sagAyakDizi;
                solAyakDizi = getMirror(sagAyakDizi);
                pictureBox1.Image = DizidenBitmape(buyutucu(solAyakDizi));
            }
            else if (seciliAyak == "sol")
            {
                sagAyakDiziEski = sagAyakDizi;
                solAyakDiziEski = solAyakDizi;
                sagAyakDizi = getMirror(solAyakDizi);
                pictureBox2.Image = DizidenBitmape(buyutucu(sagAyakDizi));
            }
            else
            {
                MessageBox.Show("Lütfen Ayna Görüntüsünü Almak İstediğiniz Ayağı Seçiniz", "Hata");
            }
        }
        //Sol ayağı diziye göre günceller
        private void pictureBox1guncelle()
        {
            maskSolAyak();
            pictureBox1.Image = DizidenBitmape(buyutucu(solAyakDizi));
        }
        //Sağ ayağı diziye göre günceller
        private void pictureBox2guncelle()
        {
            maskSagAyak();
            pictureBox2.Image = DizidenBitmape(buyutucu(sagAyakDizi));
        }
        //Geri al komutunu çağırır
        private void btn_gerial_Click(object sender, EventArgs e)
        {
            geriAl();
            if (lpgYapildi)
            {
                if (seciliAyak == "sol")
                {
                    timer.Enabled = true;
                }
                else if (seciliAyak == "sag")
                {
                    timer2.Enabled = true;
                }
                lpgYapildi = false;
            }
        }
        //Son yapılan işlemi geri alır
        private void geriAl()
        {
            if (dosyaSecili)
            {
                solAyakDizi = solAyakDiziEski;
                sagAyakDizi = sagAyakDiziEski;
                pictureBox1guncelle();
                pictureBox2guncelle();
            }
        }
        //Sol ayağa tıklandığında çağırılır. Sol ayağı seçer
        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {

                case MouseButtons.Left:
                    if (seciliAyak == "sol")
                    {

                    }
                    else
                    {
                        if (dosyaSecili)
                        {
                            seciliAyak = "sol";
                            pb1_border.BackColor = Color.Lime;
                            pb2_border.BackColor = SystemColors.Control;
                            lastPoint.X = 259;
                            lastPoint.Y = 259;
                            lbl_sag.Text = "";
                            timer2.Enabled = false;
                            if (!lpgYapildi)
                            {
                                timer.Enabled = true;
                            }

                        }
                    }
                    break;
                case MouseButtons.Right:
                    geriAl();
                    break;



            }

        }
        //Sağ ayağa tıklandığında çağırılır. Sağ ayağı seçer
        private void pictureBox2_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {

                case MouseButtons.Left:
                    if (seciliAyak == "sag")
                    {

                    }
                    else
                    {
                        if (dosyaSecili)
                        {
                            seciliAyak = "sag";
                            pb1_border.BackColor = SystemColors.Control;
                            pb2_border.BackColor = Color.Lime;
                            lastPoint.X = 259;
                            lastPoint.Y = 259;
                            lbl_sol.Text = "";
                            timer.Enabled = false;
                            if (!lpgYapildi)
                            {
                                timer2.Enabled = true;
                            }
                        }
                    }
                    break;
                case MouseButtons.Right:
                    geriAl();
                    break;



            }

        }
        //Esc tuşuna basıldığında geri alma komutunu çağırır
        private void Anaekran_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                geriAl();
            }

        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void Anaekran_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
                case MouseButtons.Left:
                    seciliAyak= "";
                    pb1_border.BackColor = SystemColors.Control;
                    pb2_border.BackColor = SystemColors.Control;
                    timer.Enabled = false;
                    timer2.Enabled = false;
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void lbl_hastakodu_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void lbl_solayaktanisitext_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void lbl_solayaktaniseviyesitext_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void lbl_sagayaktanisitext_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void lbl_sagayaktaniseviyesitext_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void lbl_scaletext_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void lbl_filtrelertext_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void label1_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void btn_dosyaac_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void btn_dosyakaydet_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void btn_yenidosyakaydet_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void btn_mirror_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void cb_alan_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void lbl_alan_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void cb_lpm_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void btn_lpm_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void cb_lpg_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void btn_lpg_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void btn_gerial_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void label2_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void label3_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void pb2_border_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void pb1_border_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    geriAl();
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    if (seciliAyak == "sol")
                    {
                        pixelSayaci = 0;
                        solAyakDiziEski = (Byte[,])solAyakDizi.Clone();
                        sagAyakDiziEski = (Byte[,])sagAyakDizi.Clone();
                        lastPoint = e.Location;
                        isMouseDown = true;
                        lastPoint.X = lastPoint.X / 8;
                        lastPoint.Y = lastPoint.Y / 8;
                        lbl_sol.Text = Convert.ToString(lastPoint.X) + "," + Convert.ToString(lastPoint.Y) + "-" + Convert.ToString(solAyakDizi[lastPoint.Y, lastPoint.X]);
                        seciliRenk = ortalamaHesapla();

                    }
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    if (seciliAyak == "sag")
                    {
                        
                        sagAyakDiziEski = (Byte[,])sagAyakDizi.Clone();
                        solAyakDiziEski = (Byte[,])solAyakDizi.Clone();
                        lastPoint = e.Location;
                        isMouseDown = true;
                        lastPoint.X = lastPoint.X / 8;
                        lastPoint.Y = lastPoint.Y / 8;
                        lbl_sag.Text = Convert.ToString(lastPoint.X) + "," + Convert.ToString(lastPoint.Y) + "-" + Convert.ToString(sagAyakDizi[lastPoint.Y, lastPoint.X]);
                        seciliRenk = sagAyakDizi[e.Location.Y / 8, e.Location.X / 8];

                    }
                    break;
            }
        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {

            isMouseDown = false;

        }
        //Ekranda mouse a tıklanınca yapılacak eylemleri belirlemek için
        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {

            isMouseDown = false;
        }
        //Mouse resim üzerinde hareket ederken koordinatını alır ve gerekliyse boyama işlemi yapar
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            
            if (seciliAyak == "sol"&&lpgYapildi==false)
            {
                if (isMouseDown && e.Location.X < 224 && e.Location.Y < 480 && e.Location.X >= 32 && e.Location.Y >= 32)
                {
                    if (pixelSayaci > 7)
                    {
                        for (int i = e.Location.Y / 8 - cikarmaKatsayisi; i <= e.Location.Y / 8 + cikarmaKatsayisi; i++)
                        {
                            for (int j = e.Location.X / 8 - cikarmaKatsayisi; j <= e.Location.X / 8 + cikarmaKatsayisi; j++)
                            {
                                solAyakDizi[i, j] = seciliRenk;
                            }
                        }
                    }
                    pixelSayaci++;
                }
            }
        }
        //Mouse resim üzerinde hareket ederken koordinatını alır ve gerekliyse boyama işlemi yapar
        private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
        {
            if (seciliAyak == "sag" && lpgYapildi == false)
            {
                if (isMouseDown && e.Location.X < 224 && e.Location.Y < 480 && e.Location.X >= 32 && e.Location.Y >= 32)
                {
                    if (pixelSayaci > 7)
                    {
                        for (int i = e.Location.Y / 8 - cikarmaKatsayisi; i <= e.Location.Y / 8 + cikarmaKatsayisi; i++)
                        {
                            for (int j = e.Location.X / 8 - cikarmaKatsayisi; j <= e.Location.X / 8 + cikarmaKatsayisi; j++)
                            {
                                sagAyakDizi[i, j] = seciliRenk;
                            }
                        }
                    }
                    pixelSayaci++;
                }
            }
        }
        //Seçili pixel in alan boyutu kadar kapladığı yerin ortalamasını hesaplar
        private Byte ortalamaHesapla()
        {
            int ortalama = 0;
            if (seciliAyak == "sol")
            {
                for (int i = lastPoint.Y - cikarmaKatsayisi; i <= lastPoint.Y + cikarmaKatsayisi; i++)
                {
                    for (int j = lastPoint.X - cikarmaKatsayisi; j <= lastPoint.X + cikarmaKatsayisi; j++)
                    {
                        if (i >= 0 && j >= 0&&i<64&&j<32)
                        {
                            ortalama += Convert.ToInt32(solAyakDizi[i, j]);
                        }
                    }

                }
                ortalama /= (((cikarmaKatsayisi + 1) * 2) - 1) * (((cikarmaKatsayisi + 1) * 2) - 1);
            }
            else if (seciliAyak == "sag")
            {
                for (int i = lastPoint.Y - cikarmaKatsayisi; i <= lastPoint.Y + cikarmaKatsayisi; i++)
                {
                    for (int j = lastPoint.X - cikarmaKatsayisi; j <= lastPoint.X + cikarmaKatsayisi; j++)
                    {
                        if (i >= 0 && j >= 0 && i < 64 && j < 32)
                        {
                            ortalama += Convert.ToInt32(sagAyakDizi[i, j]);
                        }
                    }

                }
                ortalama /= (((cikarmaKatsayisi + 1) * 2) - 1) * (((cikarmaKatsayisi + 1) * 2) - 1);
            }
            return Convert.ToByte(ortalama);
        }
        //Alan seçimi
        private void cb_alan_SelectedIndexChanged(object sender, EventArgs e)
        {
            label2.Focus();
            if (cb_alan.SelectedIndex == 1)
            {
                cikarmaKatsayisi = 1;
            }
            else if (cb_alan.SelectedIndex == 2)
            {
                cikarmaKatsayisi = 2;
            }
            else if (cb_alan.SelectedIndex == 3)
            {
                cikarmaKatsayisi = 3;
            }
            else if (cb_alan.SelectedIndex == 0)
            {
                cikarmaKatsayisi = 0;
            }
        }
        //Mouse scroll etkinliğini düzenler, seçili pixel in değerini değiştirir
        private void Form1_MouseScroll(object sender, MouseEventArgs e)
        {
            if (lastPoint.X <= 256 && seciliAyak == "sag"&&lpgYapildi==false)
            {
                if (e.Delta > 0)
                {
                    for (int i = lastPoint.Y - cikarmaKatsayisi; i <= lastPoint.Y + cikarmaKatsayisi; i++)
                    {
                        for (int j = lastPoint.X - cikarmaKatsayisi; j <= lastPoint.X + cikarmaKatsayisi; j++)
                        {
                            if (sagAyakDizi[i, j] < 246)
                            {
                                sagAyakDizi[i, j] += 7;
                            }
                            else
                            {
                                sagAyakDizi[i, j] = 255;
                            }
                        }
                    }
                    seciliRenk = ortalamaHesapla();
                    lbl_sag.Text = Convert.ToString(lastPoint.X) + "," + Convert.ToString(lastPoint.Y) + "-" + Convert.ToString(seciliRenk);
                }
                else
                {
                    for (int i = lastPoint.Y - cikarmaKatsayisi; i <= lastPoint.Y + cikarmaKatsayisi; i++)
                    {
                        for (int j = lastPoint.X - cikarmaKatsayisi; j <= lastPoint.X + cikarmaKatsayisi; j++)
                        {
                            if (sagAyakDizi[i, j] > 6)
                            {
                                sagAyakDizi[i, j] -= 4;
                            }
                            else
                            {
                                sagAyakDizi[i, j] = 0;
                            }
                        }
                    }
                    seciliRenk = ortalamaHesapla();
                    lbl_sag.Text = Convert.ToString(lastPoint.X) + "," + Convert.ToString(lastPoint.Y) + "-" + Convert.ToString(seciliRenk);
                }
            }
            else if (lastPoint.X <= 256 && seciliAyak == "sol" && lpgYapildi == false)
            {
                if (e.Delta > 0)
                {
                    for (int i = lastPoint.Y - cikarmaKatsayisi; i <= lastPoint.Y + cikarmaKatsayisi; i++)
                    {
                        for (int j = lastPoint.X - cikarmaKatsayisi; j <= lastPoint.X + cikarmaKatsayisi; j++)
                        {
                            if (solAyakDizi[i, j] < 246)
                            {
                                solAyakDizi[i, j] += 7;
                            }
                            else
                            {
                                solAyakDizi[i, j] = 255;
                            }
                        }
                    }
                    seciliRenk = ortalamaHesapla();
                    lbl_sol.Text = Convert.ToString(lastPoint.X) + "," + Convert.ToString(lastPoint.Y) + "-" + Convert.ToString(seciliRenk);
                }
                else
                {
                    for (int i = lastPoint.Y - cikarmaKatsayisi; i <= lastPoint.Y + cikarmaKatsayisi; i++)
                    {
                        for (int j = lastPoint.X - cikarmaKatsayisi; j <= lastPoint.X + cikarmaKatsayisi; j++)
                        {
                            if (solAyakDizi[i, j] > 6)
                            {
                                solAyakDizi[i, j] -= 4;
                            }
                            else
                            {
                                solAyakDizi[i, j] = 0;
                            }
                        }
                    }
                    seciliRenk = ortalamaHesapla();
                    lbl_sol.Text = Convert.ToString(lastPoint.X) + "," + Convert.ToString(lastPoint.Y) + "-" + Convert.ToString(seciliRenk);
                }
            }

        }
        //Bitmap türünden değerleri alarak diziye aktarır
        public static byte[,] BitmaptenDiziye(Bitmap bmp)
        {
            Byte[,] dizi = new Byte[bmp.Height, bmp.Width];

            for (int r = 0; r < bmp.Height; r++)
            {
                for (int c = 0; c < bmp.Width; c++)
                {
                    dizi[r, c] = bmp.GetPixel(c, r).R;

                }
            }

            return dizi;
        }
        //LPM komutu seçildiğinde çalışır
        private void btn_lpm_Click(object sender, EventArgs e)
        {

            solAyakDiziEski = (Byte[,])solAyakDizi.Clone();
            sagAyakDiziEski = (Byte[,])sagAyakDizi.Clone();


            if (cb_lpm.SelectedIndex == 0)
            {
                solAyakDizi = lpm1(solAyakDizi);
                sagAyakDizi = lpm1(sagAyakDizi);
            }
            if (cb_lpm.SelectedIndex == 1)
            {
                solAyakDizi = lpm2(solAyakDizi);
                sagAyakDizi = lpm2(sagAyakDizi);
            }
            if (cb_lpm.SelectedIndex == 2)
            {
                solAyakDizi = lpm3(solAyakDizi);
                sagAyakDizi = lpm3(sagAyakDizi);
            }
            if (cb_lpm.SelectedIndex == 3)
            {
                solAyakDizi = lpm4(solAyakDizi);
                sagAyakDizi = lpm4(sagAyakDizi);
            }
            if (cb_lpm.SelectedIndex == 4)
            {
                solAyakDizi = lpm5(solAyakDizi);
                sagAyakDizi = lpm5(sagAyakDizi);
            }


            if (timer.Enabled == false)
            {
                pictureBox1guncelle();
            }
            if (timer2.Enabled == false)
            {
                pictureBox2guncelle();
            }
            
            
            
        }
        //LPG komutu seçildiğinde çalışır
        private void btn_lpg_Click(object sender, EventArgs e)
        {
            if (seciliAyak == "sol")
            {
                timer.Enabled = false;
            }
            else if (seciliAyak == "sag")
            {
                timer2.Enabled = false;
            }
            progressBar.Value = 0;
            
            progressBar.Visible = true;
            solAyakDiziEski = (Byte[,])solAyakDizi.Clone();
            sagAyakDiziEski = (Byte[,])sagAyakDizi.Clone();

            
            pictureBox1.Image = DizidenBitmape(lowpassFilter(BitmaptenDiziye((Bitmap)pictureBox1.Image),cb_lpg.SelectedIndex+1));
            pictureBox2.Image = DizidenBitmape(lowpassFilter(BitmaptenDiziye((Bitmap)pictureBox2.Image),cb_lpg.SelectedIndex + 1));
            pictureBox1.Invalidate();
            lpgYapildi = true;
            
            progressBar.Visible = false;
        }
        //Linear Regression komutu seçildiğinde çalışır
        private void btn_linearregression_Click(object sender, EventArgs e)
        {
            linearregression();
            
        }
        //Decision Tree komutu seçildiğinde çalışır
        private void btn_decisiontree_Click(object sender, EventArgs e)
        {
            decisiontree();
        }
        //Random Forest komutu seçildiğinde çalışır
        private void btn_randomforest_Click(object sender, EventArgs e)
        {
            randomforest();
        }
        //Linear Regression işlemi
        private bool linearregression()
        {
            int maxdeneme, mindeneme;
            if (int.TryParse(tb_max.Text, out maxdeneme) && int.TryParse(tb_min.Text, out mindeneme))
            {
                int max = maxdeneme, min = mindeneme;
                solAyakDiziEski = (Byte[,])solAyakDizi.Clone();
                sagAyakDiziEski = (Byte[,])sagAyakDizi.Clone();
                Random random = new Random();
                int sayac = 1, sayacSiddeti = 0;
                int number = 0, pixelsecimi = 0,i=0,j=0;
                if (cb_linearregression.SelectedIndex == 0)
                {
                    sayacSiddeti = 3;
                }
                else if (cb_linearregression.SelectedIndex == 1)
                {
                    sayacSiddeti = 2;
                }
                else if (cb_linearregression.SelectedIndex == 2)
                {
                    sayacSiddeti = 1;
                }
                for (int k = 0; k < 64; k++)
                {
                    for (int l = 3; l < 32; l++)
                    {
                        if (sayac % sayacSiddeti == 0)
                        {
                            number = random.Next(min, max);
                            pixelsecimi = random.Next(1, sayacSiddeti);
                            if (pixelsecimi %sayacSiddeti!=0) 
                            {
                                if (sayacSiddeti==3)
                                {
                                    if (pixelsecimi == 1)
                                    {
                                        if (l > 1)
                                        {
                                            i = k;
                                            j = l - 2;
                                        }
                                        
                                    }
                                    else if (pixelsecimi == 2)
                                    {
                                        if (l > 0)
                                        {
                                            i = k;
                                            j = l - 1;
                                        }
                                        
                                    }
                                }
                                else if (sayacSiddeti == 2)
                                {
                                    if (pixelsecimi == 1)
                                    {
                                        if (l > 0)
                                        {
                                            i = k;
                                            j = l - 1;
                                        }
                                        
                                    }
                                }
                            }
                            else
                            {
                                i = k;
                                j = l;
                            }
                           
                            if (solAyakDizi[i, j] + number > 255)
                            {
                                solAyakDizi[i, j] = 255;
                            }
                            else if (solAyakDizi[i, j] + number < 0)
                            {
                                solAyakDizi[i, j] = 0;
                            }
                            else
                            {
                                solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                            }
                        }
                        sayac++;
                    }
                }
                for (int k = 0; k < 64; k++)
                {
                    for (int l = 3; l < 32; l++)
                    {
                        if (sayac % sayacSiddeti == 0)
                        {
                            number = random.Next(min, max);
                            pixelsecimi = random.Next(1, sayacSiddeti);
                            if (pixelsecimi % sayacSiddeti != 0)
                            {
                                if (sayacSiddeti == 3)
                                {
                                    if (pixelsecimi == 1)
                                    {
                                        if (l > 1)
                                        {
                                            i = k;
                                            j = l - 2;
                                        }
                                        
                                    }
                                    else if (pixelsecimi == 2)
                                    {
                                        if (l > 0)
                                        {
                                            i = k;
                                            j = l - 1;
                                        }
                                       
                                    }
                                }
                                else if (sayacSiddeti == 2)
                                {
                                    if (pixelsecimi == 1)
                                    {
                                        if (l > 0)
                                        {
                                            i = k;
                                            j = l - 1;
                                        }
                                        
                                    }
                                }
                            }
                            else
                            {
                                i = k;
                                j = l;
                            }
                            if (sagAyakDizi[i, j] + number > 255)
                            {
                                sagAyakDizi[i, j] = 255;
                            }
                            else if (sagAyakDizi[i, j] + number < 0)
                            {
                                sagAyakDizi[i, j] = 0;
                            }
                            else
                            {
                                sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                            }
                        }
                        sayac++;
                    }
                }
                pictureBox1guncelle();
                pictureBox2guncelle();
                return true;
            }
            else
            {
                MessageBox.Show("Lütfen geçerli bir minimum ve maksimum değeri giriniz.", "Hata");
                return false;
            }
        }
        //Decision Tree işlemi
        private bool decisiontree()
        {
            int maxdeneme, mindeneme;
            if (int.TryParse(tb_max.Text, out maxdeneme) && int.TryParse(tb_min.Text, out mindeneme))
            {
                int max = maxdeneme, min = mindeneme;
                solAyakDiziEski = (Byte[,])solAyakDizi.Clone();
                sagAyakDiziEski = (Byte[,])sagAyakDizi.Clone();
                Random random = new Random();
                int number = 0;
                int x = 0, y = 0, pay = 0;
                if (cb_decisiontree.SelectedIndex == 0)
                {
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(0, 62);
                        y = random.Next(0, 30);
                        number = random.Next(min, max);
                        for (int i = x; i <= x + 1; i++)
                        {
                            for (int j = y; j <= y + 1; j++)
                            {
                                if (solAyakDizi[i, j] + number > 255)
                                {
                                    solAyakDizi[i, j] = 255;
                                }
                                else if (solAyakDizi[i, j] + number < 0)
                                {
                                    solAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(pay, 63 - pay);
                        y = random.Next(pay, 31 - pay);
                        number = random.Next(min, max);
                        for (int i = x - pay; i <= x + pay; i++)
                        {
                            for (int j = y - pay; j <= y + pay; j++)
                            {
                                if (sagAyakDizi[i, j] + number > 255)
                                {
                                    sagAyakDizi[i, j] = 255;
                                }
                                else if (sagAyakDizi[i, j] + number < 0)
                                {
                                    sagAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                }
                else if (cb_decisiontree.SelectedIndex == 1)
                {

                    pay = 1;
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(pay, 63 - pay);
                        y = random.Next(pay, 31 - pay);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x - pay; i <= x + pay; i++)
                        {
                            for (int j = y - pay; j <= y + pay; j++)
                            {
                                if (sayac == 2 || sayac == 4 || sayac == 5 || sayac == 6 || sayac == 8)
                                {
                                    if (solAyakDizi[i, j] + number > 255)
                                    {
                                        solAyakDizi[i, j] = 255;
                                    }
                                    else if (solAyakDizi[i, j] + number < 0)
                                    {
                                        solAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                    }
                                }
                                sayac++;
                            }
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(pay, 63 - pay);
                        y = random.Next(pay, 31 - pay);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x - pay; i <= x + pay; i++)
                        {
                            for (int j = y - pay; j <= y + pay; j++)
                            {
                                if (sayac == 2 || sayac == 4 || sayac == 5 || sayac == 6 || sayac == 8)
                                {
                                    if (sagAyakDizi[i, j] + number > 255)
                                    {
                                        sagAyakDizi[i, j] = 255;
                                    }
                                    else if (sagAyakDizi[i, j] + number < 0)
                                    {
                                        sagAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                    }
                                }
                                sayac++;
                            }
                        }
                    }
                }
                else if (cb_decisiontree.SelectedIndex == 2)
                {


                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(0, 60);
                        y = random.Next(0, 28);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x; i <= x + 3; i++)
                        {
                            for (int j = y; j <= y + 3; j++)
                            {
                                if (sayac != 1 && sayac != 4 && sayac != 13 && sayac != 16)
                                {
                                    if (solAyakDizi[i, j] + number > 255)
                                    {
                                        solAyakDizi[i, j] = 255;
                                    }
                                    else if (solAyakDizi[i, j] + number < 0)
                                    {
                                        solAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                    }
                                }
                                sayac++;
                            }
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(0, 60);
                        y = random.Next(0, 28);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x; i <= x + 3; i++)
                        {
                            for (int j = y; j <= y + 3; j++)
                            {
                                if (sayac != 1 && sayac != 4 && sayac != 13 && sayac != 16)
                                {
                                    if (sagAyakDizi[i, j] + number > 255)
                                    {
                                        sagAyakDizi[i, j] = 255;
                                    }
                                    else if (sagAyakDizi[i, j] + number < 0)
                                    {
                                        sagAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                    }

                                }
                                sayac++;
                            }
                        }
                    }
                }
                else if (cb_decisiontree.SelectedIndex == 3)
                {

                    pay = 2;
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(pay, 63 - pay);
                        y = random.Next(pay, 31 - pay);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x - pay; i <= x + pay; i++)
                        {
                            for (int j = y - pay; j <= y + pay; j++)
                            {
                                if (sayac != 1 && sayac != 2 && sayac != 4 && sayac != 5 && sayac != 6 && sayac != 10 && sayac != 16 && sayac != 20 && sayac != 21 && sayac != 22 && sayac != 24 && sayac != 25)
                                {
                                    if (solAyakDizi[i, j] + number > 255)
                                    {
                                        solAyakDizi[i, j] = 255;
                                    }
                                    else if (solAyakDizi[i, j] + number < 0)
                                    {
                                        solAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                    }
                                }
                                sayac++;
                            }
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(pay, 63 - pay);
                        y = random.Next(pay, 31 - pay);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x - pay; i <= x + pay; i++)
                        {
                            for (int j = y - pay; j <= y + pay; j++)
                            {
                                if (sayac != 1 && sayac != 2 && sayac != 4 && sayac != 5 && sayac != 6 && sayac != 10 && sayac != 16 && sayac != 20 && sayac != 21 && sayac != 22 && sayac != 24 && sayac != 25)
                                {
                                    if (sagAyakDizi[i, j] + number > 255)
                                    {
                                        sagAyakDizi[i, j] = 255;
                                    }
                                    else if (sagAyakDizi[i, j] + number < 0)
                                    {
                                        sagAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                    }
                                }
                                sayac++;
                            }
                        }
                    }
                }
                else if (cb_decisiontree.SelectedIndex == 4)
                {
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(0, 58);
                        y = random.Next(0, 26);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x; i <= x + 5; i++)
                        {
                            for (int j = y - pay; j <= y + 5; j++)
                            {
                                if (sayac != 1 && sayac != 2 && sayac != 5 && sayac != 6 && sayac != 7 && sayac != 12 && sayac != 25 && sayac != 30 && sayac != 31 && sayac != 32 && sayac != 35 && sayac != 36)
                                {
                                    if (solAyakDizi[i, j] + number > 255)
                                    {
                                        solAyakDizi[i, j] = 255;
                                    }
                                    else if (solAyakDizi[i, j] + number < 0)
                                    {
                                        solAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                    }
                                }
                                sayac++;
                            }

                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(0, 58);
                        y = random.Next(0, 26);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x; i <= x + 5; i++)
                        {
                            for (int j = y - pay; j <= y + 5; j++)
                            {
                                if (sayac != 1 && sayac != 2 && sayac != 5 && sayac != 6 && sayac != 7 && sayac != 12 && sayac != 25 && sayac != 30 && sayac != 31 && sayac != 32 && sayac != 35 && sayac != 36)
                                {
                                    if (sagAyakDizi[i, j] + number > 255)
                                    {
                                        sagAyakDizi[i, j] = 255;
                                    }
                                    else if (sagAyakDizi[i, j] + number < 0)
                                    {
                                        sagAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                    }
                                }
                                sayac++;
                            }
                        }
                    }
                }

                pictureBox1guncelle();
                pictureBox2guncelle();
                return true;
            }
            else
            {
                MessageBox.Show("Lütfen geçerli bir minimum ve maksimum değeri giriniz.", "Hata");
                return false;
            }
        }
        //Random Forest işlemi
        private bool randomforest()
        {
            int maxdeneme, mindeneme;
            if (int.TryParse(tb_max.Text, out maxdeneme) && int.TryParse(tb_min.Text, out mindeneme))
            {
                int max = maxdeneme, min = mindeneme;
                solAyakDiziEski = (Byte[,])solAyakDizi.Clone();
                sagAyakDiziEski = (Byte[,])sagAyakDizi.Clone();
                Random random = new Random();

                int number = 0;
                int x = 0, y = 0;
                int payx = 0, payy = 0;

                if (cb_randomforest.SelectedIndex == 0)
                {

                    payx = 2;
                    payy = 2;
                    for (int k = 0; k < 50; k++)
                    {
                        int sayac = 0;
                        int satir = 1;
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y + payy; j >= y - payy; j--)
                            {
                                if (satir == 1 || satir == 5)
                                {
                                    if (sayac < 1)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 2 || satir == 4)
                                {
                                    if (sayac < 3)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 3)
                                {
                                    if (sayac < 5)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                            }
                            satir++;
                            sayac = 0;
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        int sayac = 0;
                        int satir = 1;
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y + payy; j >= y - payy; j--)
                            {
                                if (satir == 1 || satir == 5)
                                {
                                    if (sayac < 1)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 2 || satir == 4)
                                {
                                    if (sayac < 3)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 3)
                                {
                                    if (sayac < 5)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                            }
                            satir++;
                            sayac = 0;
                        }
                    }
                }
                else if (cb_randomforest.SelectedIndex == 1)
                {

                    payx = 2;
                    payy = 1;
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y - payy; j <= y + payy; j++)
                            {
                                if (solAyakDizi[i, j] + number > 255)
                                {
                                    solAyakDizi[i, j] = 255;
                                }
                                else if (solAyakDizi[i, j] + number < 0)
                                {
                                    solAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y - payy; j <= y + payy; j++)
                            {
                                if (sagAyakDizi[i, j] + number > 255)
                                {
                                    sagAyakDizi[i, j] = 255;
                                }
                                else if (sagAyakDizi[i, j] + number < 0)
                                {
                                    sagAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                }
                else if (cb_randomforest.SelectedIndex == 2)
                {

                    payx = 2;
                    payy = 2;
                    for (int k = 0; k < 50; k++)
                    {
                        int sayac = 0;
                        int satir = 1;
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y + payy; j >= y - payy; j--)
                            {
                                if (satir == 1 || satir == 5)
                                {
                                    if (sayac < 1)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 2 || satir == 4)
                                {
                                    if (sayac < 3)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 3)
                                {
                                    if (sayac < 5)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                            }
                            satir++;
                            sayac = 0;
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        int sayac = 0;
                        int satir = 1;
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y + payy; j >= y - payy; j--)
                            {
                                if (satir == 1 || satir == 5)
                                {
                                    if (sayac < 1)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 2 || satir == 4)
                                {
                                    if (sayac < 3)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 3)
                                {
                                    if (sayac < 5)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                            }
                            satir++;
                            sayac = 0;
                        }
                    }
                }
                else if (cb_randomforest.SelectedIndex == 3)
                {

                    payx = 3;
                    payy = 2;
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y - payy; j <= y + payy; j++)
                            {
                                if (solAyakDizi[i, j] + number > 255)
                                {
                                    solAyakDizi[i, j] = 255;
                                }
                                else if (solAyakDizi[i, j] + number < 0)
                                {
                                    solAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y - payy; j <= y + payy; j++)
                            {
                                if (sagAyakDizi[i, j] + number > 255)
                                {
                                    sagAyakDizi[i, j] = 255;
                                }
                                else if (sagAyakDizi[i, j] + number < 0)
                                {
                                    sagAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                }
                else if (cb_randomforest.SelectedIndex == 4)
                {

                    payx = 3;
                    payy = 3;
                    for (int k = 0; k < 50; k++)
                    {
                        int sayac = 0;
                        int satir = 1;
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y + payy; j >= y - payy; j--)
                            {
                                if (satir == 1 || satir == 7)
                                {
                                    if (sayac < 1)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 2 || satir == 6)
                                {
                                    if (sayac < 3)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 3 || satir == 5)
                                {
                                    if (sayac < 5)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 4)
                                {
                                    if (sayac < 7)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                            }
                            satir++;
                            sayac = 0;
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        int sayac = 0;
                        int satir = 1;
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y + payy; j >= y - payy; j--)
                            {
                                if (satir == 1 || satir == 7)
                                {
                                    if (sayac < 1)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 2 || satir == 6)
                                {
                                    if (sayac < 3)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 3 || satir == 5)
                                {
                                    if (sayac < 5)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 4)
                                {
                                    if (sayac < 7)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                            }
                            satir++;
                            sayac = 0;
                        }
                    }
                }
                else if (cb_randomforest.SelectedIndex == 5)
                {

                    payx = 4;
                    payy = 3;
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y - payy; j <= y + payy; j++)
                            {
                                if (solAyakDizi[i, j] + number > 255)
                                {
                                    solAyakDizi[i, j] = 255;
                                }
                                else if (solAyakDizi[i, j] + number < 0)
                                {
                                    solAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y - payy; j <= y + payy; j++)
                            {
                                if (sagAyakDizi[i, j] + number > 255)
                                {
                                    sagAyakDizi[i, j] = 255;
                                }
                                else if (sagAyakDizi[i, j] + number < 0)
                                {
                                    sagAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                }

                pictureBox1guncelle();
                pictureBox2guncelle();
                return true;
            }
            else
            {
                MessageBox.Show("Lütfen geçerli bir minimum ve maksimum değeri giriniz.", "Hata");
                return false;
            }
        }
        //Linear Regression işlemi
        private bool linearregression2()
        {
            int maxdeneme, mindeneme;
            if (int.TryParse(tb_max.Text, out maxdeneme) && int.TryParse(tb_min.Text, out mindeneme))
            {
                int max = maxdeneme, min = mindeneme;
                Random random = new Random();
                int sayac = 1, sayacSiddeti = 0;
                int number = 0;
                if (cb_linearregression.SelectedIndex == 0)
                {
                    sayacSiddeti = 3;
                }
                else if (cb_linearregression.SelectedIndex == 1)
                {
                    sayacSiddeti = 2;
                }
                else if (cb_linearregression.SelectedIndex == 2)
                {
                    sayacSiddeti = 1;
                }
                for (int i = 0; i < 64; i++)
                {
                    for (int j = 0; j < 32; j++)
                    {
                        if (sayac % sayacSiddeti == 0)
                        {
                            number = random.Next(min, max);
                            if (solAyakDizi[i, j] + number > 255)
                            {
                                solAyakDizi[i, j] = 255;
                            }
                            else if (solAyakDizi[i, j] + number < 0)
                            {
                                solAyakDizi[i, j] = 0;
                            }
                            else
                            {
                                solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                            }
                        }
                        sayac++;
                    }
                }
                for (int i = 0; i < 64; i++)
                {
                    for (int j = 0; j < 32; j++)
                    {
                        if (sayac % sayacSiddeti == 0)
                        {
                            number = random.Next(min, max);
                            if (sagAyakDizi[i, j] + number > 255)
                            {
                                sagAyakDizi[i, j] = 255;
                            }
                            else if (sagAyakDizi[i, j] + number < 0)
                            {
                                sagAyakDizi[i, j] = 0;
                            }
                            else
                            {
                                sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                            }
                        }
                        sayac++;
                    }
                }
                pictureBox1guncelle();
                pictureBox2guncelle();
                return true;
            }
            else
            {
                MessageBox.Show("Lütfen geçerli bir minimum ve maksimum değeri giriniz.", "Hata");
                return false;
            }
        }
        //Decision Tree işlemi
        private bool decisiontree2()
        {
            int maxdeneme, mindeneme;
            if (int.TryParse(tb_max.Text, out maxdeneme) && int.TryParse(tb_min.Text, out mindeneme))
            {
                int max = maxdeneme, min = mindeneme;
                Random random = new Random();
                int number = 0;
                int x = 0, y = 0, pay = 0;
                if (cb_decisiontree.SelectedIndex == 0)
                {
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(0, 62);
                        y = random.Next(0, 30);
                        number = random.Next(min, max);
                        for (int i = x; i <= x + 1; i++)
                        {
                            for (int j = y; j <= y + 1; j++)
                            {
                                if (solAyakDizi[i, j] + number > 255)
                                {
                                    solAyakDizi[i, j] = 255;
                                }
                                else if (solAyakDizi[i, j] + number < 0)
                                {
                                    solAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(pay, 63 - pay);
                        y = random.Next(pay, 31 - pay);
                        number = random.Next(min, max);
                        for (int i = x - pay; i <= x + pay; i++)
                        {
                            for (int j = y - pay; j <= y + pay; j++)
                            {
                                if (sagAyakDizi[i, j] + number > 255)
                                {
                                    sagAyakDizi[i, j] = 255;
                                }
                                else if (sagAyakDizi[i, j] + number < 0)
                                {
                                    sagAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                }
                else if (cb_decisiontree.SelectedIndex == 1)
                {

                    pay = 1;
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(pay, 63 - pay);
                        y = random.Next(pay, 31 - pay);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x - pay; i <= x + pay; i++)
                        {
                            for (int j = y - pay; j <= y + pay; j++)
                            {
                                if (sayac == 2 || sayac == 4 || sayac == 5 || sayac == 6 || sayac == 8)
                                {
                                    if (solAyakDizi[i, j] + number > 255)
                                    {
                                        solAyakDizi[i, j] = 255;
                                    }
                                    else if (solAyakDizi[i, j] + number < 0)
                                    {
                                        solAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                    }
                                }
                                sayac++;
                            }
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(pay, 63 - pay);
                        y = random.Next(pay, 31 - pay);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x - pay; i <= x + pay; i++)
                        {
                            for (int j = y - pay; j <= y + pay; j++)
                            {
                                if (sayac == 2 || sayac == 4 || sayac == 5 || sayac == 6 || sayac == 8)
                                {
                                    if (sagAyakDizi[i, j] + number > 255)
                                    {
                                        sagAyakDizi[i, j] = 255;
                                    }
                                    else if (sagAyakDizi[i, j] + number < 0)
                                    {
                                        sagAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                    }
                                }
                                sayac++;
                            }
                        }
                    }
                }
                else if (cb_decisiontree.SelectedIndex == 2)
                {


                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(0, 60);
                        y = random.Next(0, 28);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x; i <= x + 3; i++)
                        {
                            for (int j = y; j <= y + 3; j++)
                            {
                                if (sayac != 1 && sayac != 4 && sayac != 13 && sayac != 16)
                                {
                                    if (solAyakDizi[i, j] + number > 255)
                                    {
                                        solAyakDizi[i, j] = 255;
                                    }
                                    else if (solAyakDizi[i, j] + number < 0)
                                    {
                                        solAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                    }
                                }
                                sayac++;
                            }
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(0, 60);
                        y = random.Next(0, 28);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x; i <= x + 3; i++)
                        {
                            for (int j = y; j <= y + 3; j++)
                            {
                                if (sayac != 1 && sayac != 4 && sayac != 13 && sayac != 16)
                                {
                                    if (sagAyakDizi[i, j] + number > 255)
                                    {
                                        sagAyakDizi[i, j] = 255;
                                    }
                                    else if (sagAyakDizi[i, j] + number < 0)
                                    {
                                        sagAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                    }

                                }
                                sayac++;
                            }
                        }
                    }
                }
                else if (cb_decisiontree.SelectedIndex == 3)
                {

                    pay = 2;
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(pay, 63 - pay);
                        y = random.Next(pay, 31 - pay);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x - pay; i <= x + pay; i++)
                        {
                            for (int j = y - pay; j <= y + pay; j++)
                            {
                                if (sayac != 1 && sayac != 2 && sayac != 4 && sayac != 5 && sayac != 6 && sayac != 10 && sayac != 16 && sayac != 20 && sayac != 21 && sayac != 22 && sayac != 24 && sayac != 25)
                                {
                                    if (solAyakDizi[i, j] + number > 255)
                                    {
                                        solAyakDizi[i, j] = 255;
                                    }
                                    else if (solAyakDizi[i, j] + number < 0)
                                    {
                                        solAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                    }
                                }
                                sayac++;
                            }
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(pay, 63 - pay);
                        y = random.Next(pay, 31 - pay);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x - pay; i <= x + pay; i++)
                        {
                            for (int j = y - pay; j <= y + pay; j++)
                            {
                                if (sayac != 1 && sayac != 2 && sayac != 4 && sayac != 5 && sayac != 6 && sayac != 10 && sayac != 16 && sayac != 20 && sayac != 21 && sayac != 22 && sayac != 24 && sayac != 25)
                                {
                                    if (sagAyakDizi[i, j] + number > 255)
                                    {
                                        sagAyakDizi[i, j] = 255;
                                    }
                                    else if (sagAyakDizi[i, j] + number < 0)
                                    {
                                        sagAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                    }
                                }
                                sayac++;
                            }
                        }
                    }
                }
                else if (cb_decisiontree.SelectedIndex == 4)
                {
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(0, 58);
                        y = random.Next(0, 26);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x; i <= x + 5; i++)
                        {
                            for (int j = y - pay; j <= y + 5; j++)
                            {
                                if (sayac != 1 && sayac != 2 && sayac != 5 && sayac != 6 && sayac != 7 && sayac != 12 && sayac != 25 && sayac != 30 && sayac != 31 && sayac != 32 && sayac != 35 && sayac != 36)
                                {
                                    if (solAyakDizi[i, j] + number > 255)
                                    {
                                        solAyakDizi[i, j] = 255;
                                    }
                                    else if (solAyakDizi[i, j] + number < 0)
                                    {
                                        solAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                    }
                                }
                                sayac++;
                            }

                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(0, 58);
                        y = random.Next(0, 26);
                        number = random.Next(min, max);
                        int sayac = 1;
                        for (int i = x; i <= x + 5; i++)
                        {
                            for (int j = y - pay; j <= y + 5; j++)
                            {
                                if (sayac != 1 && sayac != 2 && sayac != 5 && sayac != 6 && sayac != 7 && sayac != 12 && sayac != 25 && sayac != 30 && sayac != 31 && sayac != 32 && sayac != 35 && sayac != 36)
                                {
                                    if (sagAyakDizi[i, j] + number > 255)
                                    {
                                        sagAyakDizi[i, j] = 255;
                                    }
                                    else if (sagAyakDizi[i, j] + number < 0)
                                    {
                                        sagAyakDizi[i, j] = 0;
                                    }
                                    else
                                    {
                                        sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                    }
                                }
                                sayac++;
                            }
                        }
                    }
                }

                pictureBox1guncelle();
                pictureBox2guncelle();
                return true;
            }
            else
            {
                MessageBox.Show("Lütfen geçerli bir minimum ve maksimum değeri giriniz.", "Hata");
                return false;
            }
        }
        //Random Forest işlemi
        private bool randomforest2()
        {
            int maxdeneme, mindeneme;
            if (int.TryParse(tb_max.Text, out maxdeneme) && int.TryParse(tb_min.Text, out mindeneme))
            {
                int max = maxdeneme, min = mindeneme;
                Random random = new Random();

                int number = 0;
                int x = 0, y = 0;
                int payx = 0, payy = 0;

                if (cb_randomforest.SelectedIndex == 0)
                {

                    payx = 2;
                    payy = 2;
                    for (int k = 0; k < 50; k++)
                    {
                        int sayac = 0;
                        int satir = 1;
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y + payy; j >= y - payy; j--)
                            {
                                if (satir == 1 || satir == 5)
                                {
                                    if (sayac < 1)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 2 || satir == 4)
                                {
                                    if (sayac < 3)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 3)
                                {
                                    if (sayac < 5)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                            }
                            satir++;
                            sayac = 0;
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        int sayac = 0;
                        int satir = 1;
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y + payy; j >= y - payy; j--)
                            {
                                if (satir == 1 || satir == 5)
                                {
                                    if (sayac < 1)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 2 || satir == 4)
                                {
                                    if (sayac < 3)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 3)
                                {
                                    if (sayac < 5)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                            }
                            satir++;
                            sayac = 0;
                        }
                    }
                }
                else if (cb_randomforest.SelectedIndex == 1)
                {

                    payx = 2;
                    payy = 1;
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y - payy; j <= y + payy; j++)
                            {
                                if (solAyakDizi[i, j] + number > 255)
                                {
                                    solAyakDizi[i, j] = 255;
                                }
                                else if (solAyakDizi[i, j] + number < 0)
                                {
                                    solAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y - payy; j <= y + payy; j++)
                            {
                                if (sagAyakDizi[i, j] + number > 255)
                                {
                                    sagAyakDizi[i, j] = 255;
                                }
                                else if (sagAyakDizi[i, j] + number < 0)
                                {
                                    sagAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                }
                else if (cb_randomforest.SelectedIndex == 2)
                {

                    payx = 2;
                    payy = 2;
                    for (int k = 0; k < 50; k++)
                    {
                        int sayac = 0;
                        int satir = 1;
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y + payy; j >= y - payy; j--)
                            {
                                if (satir == 1 || satir == 5)
                                {
                                    if (sayac < 1)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 2 || satir == 4)
                                {
                                    if (sayac < 3)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 3)
                                {
                                    if (sayac < 5)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                            }
                            satir++;
                            sayac = 0;
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        int sayac = 0;
                        int satir = 1;
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y + payy; j >= y - payy; j--)
                            {
                                if (satir == 1 || satir == 5)
                                {
                                    if (sayac < 1)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 2 || satir == 4)
                                {
                                    if (sayac < 3)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 3)
                                {
                                    if (sayac < 5)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                            }
                            satir++;
                            sayac = 0;
                        }
                    }
                }
                else if (cb_randomforest.SelectedIndex == 3)
                {

                    payx = 3;
                    payy = 2;
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y - payy; j <= y + payy; j++)
                            {
                                if (solAyakDizi[i, j] + number > 255)
                                {
                                    solAyakDizi[i, j] = 255;
                                }
                                else if (solAyakDizi[i, j] + number < 0)
                                {
                                    solAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y - payy; j <= y + payy; j++)
                            {
                                if (sagAyakDizi[i, j] + number > 255)
                                {
                                    sagAyakDizi[i, j] = 255;
                                }
                                else if (sagAyakDizi[i, j] + number < 0)
                                {
                                    sagAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                }
                else if (cb_randomforest.SelectedIndex == 4)
                {

                    payx = 3;
                    payy = 3;
                    for (int k = 0; k < 50; k++)
                    {
                        int sayac = 0;
                        int satir = 1;
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y + payy; j >= y - payy; j--)
                            {
                                if (satir == 1 || satir == 7)
                                {
                                    if (sayac < 1)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 2 || satir == 6)
                                {
                                    if (sayac < 3)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 3 || satir == 5)
                                {
                                    if (sayac < 5)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 4)
                                {
                                    if (sayac < 7)
                                    {
                                        if (solAyakDizi[i, j] + number > 255)
                                        {
                                            solAyakDizi[i, j] = 255;
                                        }
                                        else if (solAyakDizi[i, j] + number < 0)
                                        {
                                            solAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                            }
                            satir++;
                            sayac = 0;
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        int sayac = 0;
                        int satir = 1;
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y + payy; j >= y - payy; j--)
                            {
                                if (satir == 1 || satir == 7)
                                {
                                    if (sayac < 1)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 2 || satir == 6)
                                {
                                    if (sayac < 3)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 3 || satir == 5)
                                {
                                    if (sayac < 5)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                                if (satir == 4)
                                {
                                    if (sayac < 7)
                                    {
                                        if (sagAyakDizi[i, j] + number > 255)
                                        {
                                            sagAyakDizi[i, j] = 255;
                                        }
                                        else if (sagAyakDizi[i, j] + number < 0)
                                        {
                                            sagAyakDizi[i, j] = 0;
                                        }
                                        else
                                        {
                                            sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                        }
                                        sayac++;
                                    }
                                }
                            }
                            satir++;
                            sayac = 0;
                        }
                    }
                }
                else if (cb_randomforest.SelectedIndex == 5)
                {

                    payx = 4;
                    payy = 3;
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y - payy; j <= y + payy; j++)
                            {
                                if (solAyakDizi[i, j] + number > 255)
                                {
                                    solAyakDizi[i, j] = 255;
                                }
                                else if (solAyakDizi[i, j] + number < 0)
                                {
                                    solAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    solAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(solAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                    for (int k = 0; k < 50; k++)
                    {
                        x = random.Next(payx, 63 - payx);
                        y = random.Next(payy, 31 - payy);
                        number = random.Next(min, max);
                        for (int i = x - payx; i <= x + payx; i++)
                        {
                            for (int j = y - payy; j <= y + payy; j++)
                            {
                                if (sagAyakDizi[i, j] + number > 255)
                                {
                                    sagAyakDizi[i, j] = 255;
                                }
                                else if (sagAyakDizi[i, j] + number < 0)
                                {
                                    sagAyakDizi[i, j] = 0;
                                }
                                else
                                {
                                    sagAyakDizi[i, j] = Convert.ToByte(Convert.ToInt32(sagAyakDizi[i, j]) + number);
                                }
                            }
                        }
                    }
                }

                pictureBox1guncelle();
                pictureBox2guncelle();
                return true;
            }
            else
            {
                MessageBox.Show("Lütfen geçerli bir minimum ve maksimum değeri giriniz.", "Hata");
                return false;
            }
        }
        //Her iki görsele de low pass filtre uygular
        private Byte[,] lowpassFilter(Byte[,] ilkdizi,int siddet)
        {
            int x = siddet;
            int toplam;
            int kare = ((x * 2) + 1)*((x*2)+1);
            int sayac,sayac2=0;
            Byte[,] sondizi = new Byte[ilkdizi.GetLength(0), ilkdizi.GetLength(1)];
            for (int i = 0; i < ilkdizi.GetLength(0) - 1; i++)
            {
                for (int j = 0; j < ilkdizi.GetLength(1) - 1; j++)
                {
                    sondizi[i, j] = 0;
                }
            }

            for (int i = x; i < ilkdizi.GetLength(0) - x; i++)
            {
                for (int j = x; j < ilkdizi.GetLength(1) - x; j++)
                {
                    toplam = 0;
                    sayac = 0;
                    
                    for(int k = i - x; k <= i + x; k++)
                    {
                        for (int l = j - x; l <= j + x; l++)
                        {
                            if (ilkdizi.GetLength(0) > k && ilkdizi.GetLength(1) > l&&k>=0&&l>=0) 
                            { 
                                toplam += ilkdizi[k, l];
                                sayac++;
                            }
                        }
                    }
                    sondizi[i, j] = Convert.ToByte(toplam / sayac);
                }
                sayac2++;
                if (sayac2 %6==0 && ilkdizi.GetLength(0) == 512)
                {
                    progressBar.PerformStep();
                }
            }

            return sondizi;
        }
        //Şiddet 1 içim low pass filtre
        private static Byte[,] lpm1(Byte[,] ilkdizi)
        {
            Byte[,] sondizi = new Byte[ilkdizi.GetLength(0), ilkdizi.GetLength(1)];
            for (int i = 0; i < ilkdizi.GetLength(0) - 1; i++)
            {
                for (int j = 0; j < ilkdizi.GetLength(1) - 1; j++)
                {
                    sondizi[i, j] = 0;
                }
            }

            for (int i = 0; i < ilkdizi.GetLength(0) ; i++)
            {
                for (int j = 1; j < ilkdizi.GetLength(1) ; j++)
                {  
                    sondizi[i, j] = Convert.ToByte((ilkdizi[i,j]+ilkdizi[i,j-1])/2);
                }
                
            }

            
            return sondizi;
        }
        //Şiddet 2 içim low pass filtre
        private static Byte[,] lpm2(Byte[,] ilkdizi)
        {
            Byte[,] sondizi = new Byte[ilkdizi.GetLength(0), ilkdizi.GetLength(1)];
            for (int i = 0; i < ilkdizi.GetLength(0) - 1; i++)
            {
                for (int j = 0; j < ilkdizi.GetLength(1) - 1; j++)
                {
                    sondizi[i, j] = 0;
                }
            }

            for (int i = 0; i < ilkdizi.GetLength(0); i++)
            {
                for (int j = 1; j < ilkdizi.GetLength(1)-1; j++)
                {
                    sondizi[i, j] = Convert.ToByte((ilkdizi[i, j] + ilkdizi[i, j - 1]+ ilkdizi[i, j + 1]) / 3);
                }

            }
            return sondizi;
        }
        //Şiddet 3 içim low pass filtre
        private static Byte[,] lpm3(Byte[,] ilkdizi)
        {
            Byte[,] sondizi = new Byte[ilkdizi.GetLength(0), ilkdizi.GetLength(1)];
            for (int i = 0; i < ilkdizi.GetLength(0) - 1; i++)
            {
                for (int j = 0; j < ilkdizi.GetLength(1) - 1; j++)
                {
                    sondizi[i, j] = 0;
                }
            }

            for (int i = 1; i < ilkdizi.GetLength(0)-1; i++)
            {
                for (int j = 1; j < ilkdizi.GetLength(1) - 1; j++)
                {
                    sondizi[i, j] = Convert.ToByte((ilkdizi[i, j] + ilkdizi[i, j - 1] + ilkdizi[i, j + 1]+ilkdizi[i-1,j]+ilkdizi[i+1,j]) / 5);
                }

            }
            return sondizi;
        }
        //Şiddet 4 içim low pass filtre
        private static Byte[,] lpm4(Byte[,] ilkdizi)
        {
            Byte[,] sondizi = new Byte[ilkdizi.GetLength(0), ilkdizi.GetLength(1)];
            for (int i = 0; i < ilkdizi.GetLength(0) - 1; i++)
            {
                for (int j = 0; j < ilkdizi.GetLength(1) - 1; j++)
                {
                    sondizi[i, j] = 0;
                }
            }

            for (int i = 1; i < ilkdizi.GetLength(0) - 1; i++)
            {
                for (int j = 1; j < ilkdizi.GetLength(1) - 1; j++)
                {
                    sondizi[i, j] = Convert.ToByte((ilkdizi[i, j] + ilkdizi[i, j - 1] + ilkdizi[i, j + 1] + ilkdizi[i - 1, j] + ilkdizi[i + 1, j]+ilkdizi[i-1,j-1]+ilkdizi[i+1,j+1]) / 7);
                }

            }
            return sondizi;
        }
        //Şiddet 5 içim low pass filtre
        private static Byte[,] lpm5(Byte[,] ilkdizi)
        {
            Byte[,] sondizi = new Byte[ilkdizi.GetLength(0), ilkdizi.GetLength(1)];
            for (int i = 0; i < ilkdizi.GetLength(0) - 1; i++)
            {
                for (int j = 0; j < ilkdizi.GetLength(1) - 1; j++)
                {
                    sondizi[i, j] = 0;
                }
            }

            for (int i = 1; i < ilkdizi.GetLength(0) - 1; i++)
            {
                for (int j = 1; j < ilkdizi.GetLength(1) - 1; j++)
                {
                    sondizi[i, j] = Convert.ToByte((ilkdizi[i, j] + ilkdizi[i, j - 1] + ilkdizi[i, j + 1] + ilkdizi[i - 1, j] + ilkdizi[i + 1, j] + ilkdizi[i - 1, j - 1] + ilkdizi[i + 1, j + 1] + ilkdizi[i - 1, j + 1] + ilkdizi[i + 1, j - 1]) / 9);
                }

            }
            return sondizi;
        }
        //Dynamic Range komutu seçildiğinde çalışır
        private void btn_dynamicrange_Click(object sender, EventArgs e)
        {

            solAyakDiziEski = (Byte[,])solAyakDizi.Clone();
            sagAyakDiziEski = (Byte[,])sagAyakDizi.Clone();
            for(int i = 0; i < 64; i++)
            {
                for(int j = 0; j < 32; j++)
                {
                    
                    if (solAyakDizi[i, j] < 11)
                    {
                        solAyakDizi[i, j] = 0;
                    }
                    else if(solAyakDizi[i, j] < 21)
                    {
                        solAyakDizi[i, j] += 3;
                    }
                    else if (solAyakDizi[i, j] < 31)
                    {
                        solAyakDizi[i, j] += 4;
                    }
                    else if (solAyakDizi[i, j] < 41)
                    {
                        solAyakDizi[i, j] += 5;
                    }
                    else if (solAyakDizi[i, j] < 151)
                    {

                    }
                    else if (solAyakDizi[i, j] < 201)
                    {
                        solAyakDizi[i, j] += 6;
                    }
                    else if (solAyakDizi[i, j] < 221)
                    {
                        solAyakDizi[i, j] += 7;
                    }
                    else if (solAyakDizi[i, j] < 231)
                    {
                        solAyakDizi[i, j] += 8;
                    }
                }

            }
            for (int i = 0; i < 64; i++)
            {
                for (int j = 0; j < 32; j++)
                {
                    
                    if (sagAyakDizi[i, j] < 11)
                    {
                        sagAyakDizi[i, j] = 0;
                    }
                    else if (sagAyakDizi[i, j] < 21)
                    {
                        sagAyakDizi[i, j] += 3;
                    }
                    else if (sagAyakDizi[i, j] < 31)
                    {
                        sagAyakDizi[i, j] += 4;
                    }
                    else if (sagAyakDizi[i, j] < 41)
                    {
                        sagAyakDizi[i, j] += 5;
                    }
                    else if (sagAyakDizi[i, j] < 151)
                    {

                    }
                    else if (sagAyakDizi[i, j] < 201)
                    {
                        sagAyakDizi[i, j] += 6;
                    }
                    else if (sagAyakDizi[i, j] < 221)
                    {
                        sagAyakDizi[i, j] += 7;
                    }
                    else if (sagAyakDizi[i, j] < 231)
                    {
                        sagAyakDizi[i, j] += 8;
                    }
                }

            }
            pictureBox1guncelle();
            pictureBox2guncelle();

        }
        //Dynamic Range komutu seçildiğinde çalışır
        private void btn_histogrameq_Click(object sender, EventArgs e)
        {

            solAyakDiziEski = (Byte[,])solAyakDizi.Clone();
            sagAyakDiziEski = (Byte[,])sagAyakDizi.Clone();
            for (int i = 0; i < 64; i++)
            {
                for (int j = 0; j < 32; j++)
                {

                    if (solAyakDizi[i, j] < 21)
                    {
                        
                    }
                    else if (solAyakDizi[i, j] < 31)
                    {
                        solAyakDizi[i, j] -= 2;
                    }
                    else if (solAyakDizi[i, j] < 41)
                    {
                        solAyakDizi[i, j] -= 3;
                    }
                    else if (solAyakDizi[i, j] < 51)
                    {
                        solAyakDizi[i, j] -= 4;
                    }
                    else if (solAyakDizi[i, j] < 151)
                    {

                    }
                    else if (solAyakDizi[i, j] < 201)
                    {
                        solAyakDizi[i, j] += 2;
                    }
                    else if (solAyakDizi[i, j] < 221)
                    {
                        solAyakDizi[i, j] += 3;
                    }
                    else if (solAyakDizi[i, j] < 231)
                    {
                        solAyakDizi[i, j] += 4;
                    }
                }

            }
            for (int i = 0; i < 64; i++)
            {
                for (int j = 0; j < 32; j++)
                {

                    if (sagAyakDizi[i, j] < 21)
                    {

                    }
                    else if (sagAyakDizi[i, j] < 31)
                    {
                        sagAyakDizi[i, j] -= 2;
                    }
                    else if (sagAyakDizi[i, j] < 41)
                    {
                        sagAyakDizi[i, j] -= 3;
                    }
                    else if (sagAyakDizi[i, j] < 51)
                    {
                        sagAyakDizi[i, j] -= 4;
                    }
                    else if (sagAyakDizi[i, j] < 151)
                    {

                    }
                    else if (sagAyakDizi[i, j] < 201)
                    {
                        sagAyakDizi[i, j] += 2;
                    }
                    else if (sagAyakDizi[i, j] < 221)
                    {
                        sagAyakDizi[i, j] += 3;
                    }
                    else if (sagAyakDizi[i, j] < 231)
                    {
                        sagAyakDizi[i, j] += 4;
                    }
                }

            }
            pictureBox1guncelle();
            pictureBox2guncelle();
        }
        //Ekran arkaplandayken sayaçları durdurur
        private void Anaekran_Deactivate(object sender, EventArgs e)
        {
            if (timer.Enabled == true)
            {
                timerflag = true;
                timer.Enabled = false;
            }
            if (timer2.Enabled == true)
            {
                timer2flag = true;
                timer2.Enabled = false;
            }
        }
        //Ekran önplandayken sayaçları başlatır
        private void Anaekran_Activated(object sender, EventArgs e)
        {
            if (timerflag == true)
            {
                timer.Enabled = true;
            }
            if (timer2flag == true)
            {
                timer2.Enabled = true;
            }
        }
        //Dili Türkçe yapar
        private void btn_turkish_Click(object sender, EventArgs e)
        {
            btn_dosyaac.Text = "Dosya Yükle";
            btn_dosyakaydet.Text = "Dosya Kaydet";
            btn_yenidosyakaydet.Text = "Yeni Dosya Kaydet";
            lbl_alan.Text = "Alan:";
            lbl_randomsiddeti.Text = "Random Şiddeti";
            btn_gerial.Text = "Geri Al";
            label2.Text = "Sol Ayak";
            label3.Text = "Sağ Ayak";
            label4.Text = "BAKAS BİLİŞİM Baropodometric Kullanıcı Arayüzü";
            Anaekran.ActiveForm.Text = "Bakas Bilişim";
            label8.Text = "SOL AYAK TANISI:";
            label7.Text = "SOL AYAK TANI SEVİYESİ:";
            label6.Text = "SOL AYAK TANIMLANAN HASTA TANI GRUBU: ";
            label5.Text = "SOL AYAK TANIMLANAN HASTA TANI SEVİYESİ:";
            label12.Text = "SAĞ AYAK TANISI:";
            label11.Text = "SAĞ AYAK TANI SEVİYESİ:";
            label10.Text = "SAĞ AYAK TANIMLANAN HASTA TANI GRUBU:";
            label9.Text = "SAĞ AYAK TANIMLANAN HASTA TANI SEVİYESİ:";
            label15.Text = "HASTA KODU:";
            label13.Text = "FİLTRELER:";
        }
        //Dili İngilizce yapar
        private void btn_english_Click(object sender, EventArgs e)
        {
            btn_dosyaac.Text = "Load File";
            btn_dosyakaydet.Text = "Save File";
            btn_yenidosyakaydet.Text = "Save As New File";
            lbl_alan.Text = "Area:";
            lbl_randomsiddeti.Text = "Random Intensity";
            btn_gerial.Text = "Undo";
            label2.Text = "Left Foot";
            label3.Text = "Right Foot";
            label4.Text = "BAKAS BILISIM IT Baropodometric Analysis for Children";
            Anaekran.ActiveForm.Text = "Bakas IT";
            label8.Text = "LEFT FOOT DIAGNOSIS:";
            label7.Text = "LEFT FOOT DIAGNOSIS LEVEL:";
            label6.Text = "LEFT FOOT DEFINED PATIENT DIAGNOSIS GROUP:";
            label5.Text = "LEFT FOOT DEFINED PATIENT DIAGNOSIS LEVEL:";
            label12.Text = "RIGHT FOOT DIAGNOSIS:";
            label11.Text = "RIGHT FOOT DIAGNOSIS LEVEL:";
            label10.Text = "RIGHT FOOT DEFINED PATIENT DIAGNOSIS GROUP:";
            label9.Text = "RIGHT FOOT DEFINED PATIENT DIAGNOSIS LEVEL:";
            label15.Text = "PATIENT CODE:";
            label13.Text = "FILTERS:";
        }

        //Batch komutu seçildiğinde çalışır
        private void btn_batch_Click(object sender, EventArgs e)
        {
            String hastakoduEski = hastaKodu;
            progressBar.Value = 0;
            progressBar.Visible = true;
            int tekrar = 0;
            if (cb_batch2.SelectedIndex == 0)
            {
                tekrar = 100;
            }
            else if (cb_batch2.SelectedIndex == 1)
            {
                tekrar = 500;
            }
            else if (cb_batch2.SelectedIndex == 2)
            {
                tekrar = 1000;
            }


            if (cb_batch1.SelectedIndex == 0)
            {
                solAyakDiziEski = (Byte[,])solAyakDizi.Clone();
                sagAyakDiziEski = (Byte[,])sagAyakDizi.Clone();
                
                if (linearregression2())
                {
                    newscale();
                    dizidenYeniDosyaya(solAyakDizi, "sol");
                    dizidenYeniDosyaya(sagAyakDizi, "sag");
                    hastaKodu = hastaKodu.Substring(0, hastaKodu.Length - 6) + hastaKoduOlustur();
                    progressBar.PerformStep();
                    for (int i = 0; i < tekrar - 1; i++)
                    {
                        if (kaydet == false)
                        {
                            hastaKodu = hastakoduEski;
                            kaydet = true;
                            dosyaVarSayac = 0;
                            return ;
                        }
                        geriAl();
                        linearregression2();
                        newscale();
                        dizidenYeniDosyaya(solAyakDizi, "sol");
                        dizidenYeniDosyaya(sagAyakDizi, "sag");
                        hastaKodu = hastaKodu.Substring(0, hastaKodu.Length - 6) + hastaKoduOlustur();
                        if (tekrar == 100)
                        {
                            progressBar.PerformStep();
                        }
                        else if (tekrar == 500)
                        {
                            if (i % 5 == 0)
                            {
                                progressBar.PerformStep();
                            }
                        }
                        else if (tekrar == 1000)
                        {
                            if (i % 10 == 0)
                            {
                                progressBar.PerformStep();
                            }
                        }
                    }
                }
                
            }
            else if (cb_batch1.SelectedIndex == 1)
            {
                solAyakDiziEski = (Byte[,])solAyakDizi.Clone();
                sagAyakDiziEski = (Byte[,])sagAyakDizi.Clone();
                if (decisiontree2())
                {
                    newscale();
                    dizidenYeniDosyaya(solAyakDizi, "sol");
                    dizidenYeniDosyaya(sagAyakDizi, "sag");
                    hastaKodu = hastaKodu.Substring(0, hastaKodu.Length - 6) + hastaKoduOlustur();
                    progressBar.PerformStep();
                    for (int i = 0; i < tekrar - 1; i++)
                    {
                        if (kaydet == false)
                        {
                            hastaKodu = hastakoduEski;
                            kaydet = true;
                            dosyaVarSayac = 0;
                            return ;
                        }
                        geriAl();
                        decisiontree2(); 
                        newscale();
                        dizidenYeniDosyaya(solAyakDizi, "sol");
                        dizidenYeniDosyaya(sagAyakDizi, "sag");
                        hastaKodu = hastaKodu.Substring(0, hastaKodu.Length - 6) + hastaKoduOlustur();
                        if (tekrar == 100)
                        {
                            progressBar.PerformStep();
                        }
                        else if (tekrar == 500)
                        {
                            if (i % 5 == 0)
                            {
                                progressBar.PerformStep();
                            }
                        }
                        else if (tekrar == 1000)
                        {
                            if (i % 10 == 0)
                            {
                                progressBar.PerformStep();
                            }
                        }
                    }
                    
                }
                
            }//
            else if (cb_batch1.SelectedIndex == 2)
            {
                solAyakDiziEski = (Byte[,])solAyakDizi.Clone();
                sagAyakDiziEski = (Byte[,])sagAyakDizi.Clone();
                if (randomforest2())
                {
                    newscale();
                    dizidenYeniDosyaya(solAyakDizi, "sol");
                    dizidenYeniDosyaya(sagAyakDizi, "sag");
                    hastaKodu = hastaKodu.Substring(0, hastaKodu.Length - 6) + hastaKoduOlustur();
                    progressBar.PerformStep();
                    for (int i = 0; i < tekrar - 1; i++)
                    {
                        if (kaydet == false)
                        {
                            hastaKodu = hastakoduEski;
                            kaydet = true;
                            dosyaVarSayac = 0;
                            return ;
                        }
                        geriAl();
                        randomforest2();
                        newscale();
                        dizidenYeniDosyaya(solAyakDizi, "sol");
                        dizidenYeniDosyaya(sagAyakDizi, "sag");
                        hastaKodu = hastaKodu.Substring(0, hastaKodu.Length - 6) + hastaKoduOlustur();
                        if (tekrar == 100)
                        {
                            progressBar.PerformStep();
                        }
                        else if (tekrar == 500)
                        {
                            if (i % 5 == 0)
                            {
                                progressBar.PerformStep();
                            }
                        }
                        else if (tekrar == 1000)
                        {
                            if (i % 10 == 0)
                            {
                                progressBar.PerformStep();
                            }
                        }
                    }
                    
                }
                
                
            }
            progressBar.Visible = false;
            hastaKodu = hastakoduEski;
            kaydet = true;
            dosyaVarSayac = 0;
            geriAl();
        }
    }
}
