﻿namespace Baropodometrik
{
    partial class Anaekran
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_histogrameq = new System.Windows.Forms.Button();
            this.cb_batch2 = new System.Windows.Forms.ComboBox();
            this.btn_dynamicrange = new System.Windows.Forms.Button();
            this.lbl_max = new System.Windows.Forms.Label();
            this.cb_batch1 = new System.Windows.Forms.ComboBox();
            this.btn_batch = new System.Windows.Forms.Button();
            this.check_despeckle = new System.Windows.Forms.CheckBox();
            this.lbl_min = new System.Windows.Forms.Label();
            this.btn_decisiontree = new System.Windows.Forms.Button();
            this.lbl_despeckle = new System.Windows.Forms.Label();
            this.btn_dosyaac = new System.Windows.Forms.Button();
            this.tb_min = new System.Windows.Forms.TextBox();
            this.tb_max = new System.Windows.Forms.TextBox();
            this.cb_decisiontree = new System.Windows.Forms.ComboBox();
            this.cb_linearregression = new System.Windows.Forms.ComboBox();
            this.lbl_randomsiddeti = new System.Windows.Forms.Label();
            this.btn_linearregression = new System.Windows.Forms.Button();
            this.cb_randomforest = new System.Windows.Forms.ComboBox();
            this.btn_randomforest = new System.Windows.Forms.Button();
            this.btn_yenidosyakaydet = new System.Windows.Forms.Button();
            this.btn_gerial = new System.Windows.Forms.Button();
            this.btn_lpg = new System.Windows.Forms.Button();
            this.cb_lpg = new System.Windows.Forms.ComboBox();
            this.cb_lpm = new System.Windows.Forms.ComboBox();
            this.lbl_alan = new System.Windows.Forms.Label();
            this.btn_lpm = new System.Windows.Forms.Button();
            this.cb_alan = new System.Windows.Forms.ComboBox();
            this.btn_dosyakaydet = new System.Windows.Forms.Button();
            this.btn_mirror = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_hastakodu = new System.Windows.Forms.Label();
            this.lbl_solayaktanisitext = new System.Windows.Forms.Label();
            this.lbl_solayaktaniseviyesitext = new System.Windows.Forms.Label();
            this.lbl_sagayaktanisitext = new System.Windows.Forms.Label();
            this.lbl_sagayaktaniseviyesitext = new System.Windows.Forms.Label();
            this.lbl_scaletext = new System.Windows.Forms.Label();
            this.lbl_filtrelertext = new System.Windows.Forms.Label();
            this.pb1_border = new System.Windows.Forms.Panel();
            this.pb2_border = new System.Windows.Forms.Panel();
            this.lbl_sol = new System.Windows.Forms.Label();
            this.lbl_sag = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.lbl_soltanimseviyesi = new System.Windows.Forms.Label();
            this.lbl_soltanimgrubu = new System.Windows.Forms.Label();
            this.lbl_sagtanimseviyesi = new System.Windows.Forms.Label();
            this.lbl_sagtanimgrubu = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_english = new System.Windows.Forms.Button();
            this.btn_turkish = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pb1_border.SuspendLayout();
            this.pb2_border.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btn_histogrameq);
            this.panel1.Controls.Add(this.cb_batch2);
            this.panel1.Controls.Add(this.btn_dynamicrange);
            this.panel1.Controls.Add(this.lbl_max);
            this.panel1.Controls.Add(this.cb_batch1);
            this.panel1.Controls.Add(this.btn_batch);
            this.panel1.Controls.Add(this.check_despeckle);
            this.panel1.Controls.Add(this.lbl_min);
            this.panel1.Controls.Add(this.btn_decisiontree);
            this.panel1.Controls.Add(this.lbl_despeckle);
            this.panel1.Controls.Add(this.btn_dosyaac);
            this.panel1.Controls.Add(this.tb_min);
            this.panel1.Controls.Add(this.tb_max);
            this.panel1.Controls.Add(this.cb_decisiontree);
            this.panel1.Controls.Add(this.cb_linearregression);
            this.panel1.Controls.Add(this.lbl_randomsiddeti);
            this.panel1.Controls.Add(this.btn_linearregression);
            this.panel1.Controls.Add(this.cb_randomforest);
            this.panel1.Controls.Add(this.btn_randomforest);
            this.panel1.Controls.Add(this.btn_yenidosyakaydet);
            this.panel1.Controls.Add(this.btn_gerial);
            this.panel1.Controls.Add(this.btn_lpg);
            this.panel1.Controls.Add(this.cb_lpg);
            this.panel1.Controls.Add(this.cb_lpm);
            this.panel1.Controls.Add(this.lbl_alan);
            this.panel1.Controls.Add(this.btn_lpm);
            this.panel1.Controls.Add(this.cb_alan);
            this.panel1.Controls.Add(this.btn_dosyakaydet);
            this.panel1.Controls.Add(this.btn_mirror);
            this.panel1.Location = new System.Drawing.Point(11, 266);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(324, 514);
            this.panel1.TabIndex = 2;
            this.panel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseClick);
            // 
            // btn_histogrameq
            // 
            this.btn_histogrameq.Location = new System.Drawing.Point(3, 251);
            this.btn_histogrameq.Name = "btn_histogrameq";
            this.btn_histogrameq.Size = new System.Drawing.Size(316, 28);
            this.btn_histogrameq.TabIndex = 32;
            this.btn_histogrameq.Text = "Histogram EQ";
            this.btn_histogrameq.UseVisualStyleBackColor = true;
            this.btn_histogrameq.Click += new System.EventHandler(this.btn_histogrameq_Click);
            // 
            // cb_batch2
            // 
            this.cb_batch2.FormattingEnabled = true;
            this.cb_batch2.Items.AddRange(new object[] {
            "100",
            "500",
            "1000"});
            this.cb_batch2.Location = new System.Drawing.Point(212, 452);
            this.cb_batch2.Name = "cb_batch2";
            this.cb_batch2.Size = new System.Drawing.Size(107, 21);
            this.cb_batch2.TabIndex = 35;
            // 
            // btn_dynamicrange
            // 
            this.btn_dynamicrange.Location = new System.Drawing.Point(3, 217);
            this.btn_dynamicrange.Name = "btn_dynamicrange";
            this.btn_dynamicrange.Size = new System.Drawing.Size(316, 28);
            this.btn_dynamicrange.TabIndex = 31;
            this.btn_dynamicrange.Text = "Dynamic Range";
            this.btn_dynamicrange.UseVisualStyleBackColor = true;
            this.btn_dynamicrange.Click += new System.EventHandler(this.btn_dynamicrange_Click);
            // 
            // lbl_max
            // 
            this.lbl_max.AutoSize = true;
            this.lbl_max.Location = new System.Drawing.Point(119, 288);
            this.lbl_max.Name = "lbl_max";
            this.lbl_max.Size = new System.Drawing.Size(34, 15);
            this.lbl_max.TabIndex = 43;
            this.lbl_max.Text = "Max:";
            // 
            // cb_batch1
            // 
            this.cb_batch1.FormattingEnabled = true;
            this.cb_batch1.Items.AddRange(new object[] {
            "Linear Regression",
            "Decision Tree",
            "Random Forest"});
            this.cb_batch1.Location = new System.Drawing.Point(84, 452);
            this.cb_batch1.Name = "cb_batch1";
            this.cb_batch1.Size = new System.Drawing.Size(122, 21);
            this.cb_batch1.TabIndex = 34;
            // 
            // btn_batch
            // 
            this.btn_batch.Location = new System.Drawing.Point(3, 450);
            this.btn_batch.Name = "btn_batch";
            this.btn_batch.Size = new System.Drawing.Size(75, 23);
            this.btn_batch.TabIndex = 33;
            this.btn_batch.Text = "Batch";
            this.btn_batch.UseVisualStyleBackColor = true;
            this.btn_batch.Click += new System.EventHandler(this.btn_batch_Click);
            // 
            // check_despeckle
            // 
            this.check_despeckle.AutoSize = true;
            this.check_despeckle.Checked = true;
            this.check_despeckle.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check_despeckle.Location = new System.Drawing.Point(81, 166);
            this.check_despeckle.Name = "check_despeckle";
            this.check_despeckle.Size = new System.Drawing.Size(15, 14);
            this.check_despeckle.TabIndex = 30;
            this.check_despeckle.UseVisualStyleBackColor = true;
            // 
            // lbl_min
            // 
            this.lbl_min.AutoSize = true;
            this.lbl_min.Location = new System.Drawing.Point(225, 288);
            this.lbl_min.Name = "lbl_min";
            this.lbl_min.Size = new System.Drawing.Size(31, 15);
            this.lbl_min.TabIndex = 42;
            this.lbl_min.Text = "Min:";
            // 
            // btn_decisiontree
            // 
            this.btn_decisiontree.Location = new System.Drawing.Point(3, 348);
            this.btn_decisiontree.Name = "btn_decisiontree";
            this.btn_decisiontree.Size = new System.Drawing.Size(158, 28);
            this.btn_decisiontree.TabIndex = 36;
            this.btn_decisiontree.Text = "Decision Tree";
            this.btn_decisiontree.UseVisualStyleBackColor = true;
            this.btn_decisiontree.Click += new System.EventHandler(this.btn_decisiontree_Click);
            // 
            // lbl_despeckle
            // 
            this.lbl_despeckle.AutoSize = true;
            this.lbl_despeckle.Location = new System.Drawing.Point(4, 165);
            this.lbl_despeckle.Name = "lbl_despeckle";
            this.lbl_despeckle.Size = new System.Drawing.Size(68, 15);
            this.lbl_despeckle.TabIndex = 29;
            this.lbl_despeckle.Text = "Despeckle:";
            // 
            // btn_dosyaac
            // 
            this.btn_dosyaac.Location = new System.Drawing.Point(3, 3);
            this.btn_dosyaac.Name = "btn_dosyaac";
            this.btn_dosyaac.Size = new System.Drawing.Size(316, 28);
            this.btn_dosyaac.TabIndex = 5;
            this.btn_dosyaac.Text = "Dosya Yükle";
            this.btn_dosyaac.UseVisualStyleBackColor = true;
            this.btn_dosyaac.Click += new System.EventHandler(this.btn_dosyaac_Click);
            this.btn_dosyaac.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btn_dosyaac_MouseClick);
            // 
            // tb_min
            // 
            this.tb_min.Location = new System.Drawing.Point(266, 285);
            this.tb_min.Name = "tb_min";
            this.tb_min.Size = new System.Drawing.Size(53, 20);
            this.tb_min.TabIndex = 41;
            // 
            // tb_max
            // 
            this.tb_max.Location = new System.Drawing.Point(166, 285);
            this.tb_max.Name = "tb_max";
            this.tb_max.Size = new System.Drawing.Size(53, 20);
            this.tb_max.TabIndex = 40;
            // 
            // cb_decisiontree
            // 
            this.cb_decisiontree.FormattingEnabled = true;
            this.cb_decisiontree.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cb_decisiontree.Location = new System.Drawing.Point(167, 353);
            this.cb_decisiontree.Name = "cb_decisiontree";
            this.cb_decisiontree.Size = new System.Drawing.Size(152, 21);
            this.cb_decisiontree.TabIndex = 38;
            // 
            // cb_linearregression
            // 
            this.cb_linearregression.FormattingEnabled = true;
            this.cb_linearregression.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cb_linearregression.Location = new System.Drawing.Point(167, 319);
            this.cb_linearregression.Name = "cb_linearregression";
            this.cb_linearregression.Size = new System.Drawing.Size(152, 21);
            this.cb_linearregression.TabIndex = 37;
            // 
            // lbl_randomsiddeti
            // 
            this.lbl_randomsiddeti.AutoSize = true;
            this.lbl_randomsiddeti.Location = new System.Drawing.Point(3, 288);
            this.lbl_randomsiddeti.Name = "lbl_randomsiddeti";
            this.lbl_randomsiddeti.Size = new System.Drawing.Size(99, 15);
            this.lbl_randomsiddeti.TabIndex = 39;
            this.lbl_randomsiddeti.Text = "Random Şiddeti:";
            // 
            // btn_linearregression
            // 
            this.btn_linearregression.Location = new System.Drawing.Point(3, 314);
            this.btn_linearregression.Name = "btn_linearregression";
            this.btn_linearregression.Size = new System.Drawing.Size(158, 28);
            this.btn_linearregression.TabIndex = 35;
            this.btn_linearregression.Text = "Linear Regression";
            this.btn_linearregression.UseVisualStyleBackColor = true;
            this.btn_linearregression.Click += new System.EventHandler(this.btn_linearregression_Click);
            // 
            // cb_randomforest
            // 
            this.cb_randomforest.FormattingEnabled = true;
            this.cb_randomforest.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.cb_randomforest.Location = new System.Drawing.Point(167, 387);
            this.cb_randomforest.Name = "cb_randomforest";
            this.cb_randomforest.Size = new System.Drawing.Size(152, 21);
            this.cb_randomforest.TabIndex = 33;
            // 
            // btn_randomforest
            // 
            this.btn_randomforest.Location = new System.Drawing.Point(3, 382);
            this.btn_randomforest.Name = "btn_randomforest";
            this.btn_randomforest.Size = new System.Drawing.Size(158, 28);
            this.btn_randomforest.TabIndex = 31;
            this.btn_randomforest.Text = "Random Forest";
            this.btn_randomforest.UseVisualStyleBackColor = true;
            this.btn_randomforest.Click += new System.EventHandler(this.btn_randomforest_Click);
            // 
            // btn_yenidosyakaydet
            // 
            this.btn_yenidosyakaydet.Location = new System.Drawing.Point(3, 71);
            this.btn_yenidosyakaydet.Name = "btn_yenidosyakaydet";
            this.btn_yenidosyakaydet.Size = new System.Drawing.Size(316, 28);
            this.btn_yenidosyakaydet.TabIndex = 30;
            this.btn_yenidosyakaydet.Text = "Yeni Dosya Kaydet";
            this.btn_yenidosyakaydet.UseVisualStyleBackColor = true;
            this.btn_yenidosyakaydet.Click += new System.EventHandler(this.btn_yenidosyakaydet_Click);
            this.btn_yenidosyakaydet.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btn_yenidosyakaydet_MouseClick);
            // 
            // btn_gerial
            // 
            this.btn_gerial.Location = new System.Drawing.Point(3, 479);
            this.btn_gerial.Name = "btn_gerial";
            this.btn_gerial.Size = new System.Drawing.Size(316, 28);
            this.btn_gerial.TabIndex = 29;
            this.btn_gerial.Text = "Geri Al";
            this.btn_gerial.UseVisualStyleBackColor = true;
            this.btn_gerial.Click += new System.EventHandler(this.btn_gerial_Click);
            this.btn_gerial.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btn_gerial_MouseClick);
            // 
            // btn_lpg
            // 
            this.btn_lpg.Location = new System.Drawing.Point(3, 416);
            this.btn_lpg.Name = "btn_lpg";
            this.btn_lpg.Size = new System.Drawing.Size(158, 28);
            this.btn_lpg.TabIndex = 24;
            this.btn_lpg.Text = "LPG";
            this.btn_lpg.UseVisualStyleBackColor = true;
            this.btn_lpg.Click += new System.EventHandler(this.btn_lpg_Click);
            this.btn_lpg.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btn_lpg_MouseClick);
            // 
            // cb_lpg
            // 
            this.cb_lpg.FormattingEnabled = true;
            this.cb_lpg.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cb_lpg.Location = new System.Drawing.Point(167, 421);
            this.cb_lpg.Name = "cb_lpg";
            this.cb_lpg.Size = new System.Drawing.Size(152, 21);
            this.cb_lpg.TabIndex = 28;
            this.cb_lpg.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cb_lpg_MouseClick);
            // 
            // cb_lpm
            // 
            this.cb_lpm.FormattingEnabled = true;
            this.cb_lpm.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cb_lpm.Location = new System.Drawing.Point(167, 188);
            this.cb_lpm.Name = "cb_lpm";
            this.cb_lpm.Size = new System.Drawing.Size(152, 21);
            this.cb_lpm.TabIndex = 27;
            this.cb_lpm.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cb_lpm_MouseClick);
            // 
            // lbl_alan
            // 
            this.lbl_alan.AutoSize = true;
            this.lbl_alan.Location = new System.Drawing.Point(3, 108);
            this.lbl_alan.Name = "lbl_alan";
            this.lbl_alan.Size = new System.Drawing.Size(34, 15);
            this.lbl_alan.TabIndex = 26;
            this.lbl_alan.Text = "Alan:";
            this.lbl_alan.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lbl_alan_MouseClick);
            // 
            // btn_lpm
            // 
            this.btn_lpm.Location = new System.Drawing.Point(3, 183);
            this.btn_lpm.Name = "btn_lpm";
            this.btn_lpm.Size = new System.Drawing.Size(158, 28);
            this.btn_lpm.TabIndex = 23;
            this.btn_lpm.Text = "LPM";
            this.btn_lpm.UseVisualStyleBackColor = true;
            this.btn_lpm.Click += new System.EventHandler(this.btn_lpm_Click);
            this.btn_lpm.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btn_lpm_MouseClick);
            // 
            // cb_alan
            // 
            this.cb_alan.FormattingEnabled = true;
            this.cb_alan.Items.AddRange(new object[] {
            "1X1",
            "3X3",
            "5X5",
            "7X7"});
            this.cb_alan.Location = new System.Drawing.Point(167, 105);
            this.cb_alan.Name = "cb_alan";
            this.cb_alan.Size = new System.Drawing.Size(152, 21);
            this.cb_alan.TabIndex = 25;
            this.cb_alan.Tag = "";
            this.cb_alan.SelectedIndexChanged += new System.EventHandler(this.cb_alan_SelectedIndexChanged);
            this.cb_alan.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cb_alan_MouseClick);
            // 
            // btn_dosyakaydet
            // 
            this.btn_dosyakaydet.Location = new System.Drawing.Point(3, 37);
            this.btn_dosyakaydet.Name = "btn_dosyakaydet";
            this.btn_dosyakaydet.Size = new System.Drawing.Size(316, 28);
            this.btn_dosyakaydet.TabIndex = 21;
            this.btn_dosyakaydet.Text = "Dosya Kaydet";
            this.btn_dosyakaydet.UseVisualStyleBackColor = true;
            this.btn_dosyakaydet.Click += new System.EventHandler(this.btn_dosyakaydet_Click);
            this.btn_dosyakaydet.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btn_dosyakaydet_MouseClick);
            // 
            // btn_mirror
            // 
            this.btn_mirror.Location = new System.Drawing.Point(3, 132);
            this.btn_mirror.Name = "btn_mirror";
            this.btn_mirror.Size = new System.Drawing.Size(316, 28);
            this.btn_mirror.TabIndex = 22;
            this.btn_mirror.Text = "Mirror";
            this.btn_mirror.UseVisualStyleBackColor = true;
            this.btn_mirror.Click += new System.EventHandler(this.btn_mirror_Click);
            this.btn_mirror.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btn_mirror_MouseClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(388, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Sol Ayak";
            this.label2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.label2_MouseClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(655, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Sağ Ayak";
            this.label3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.label3_MouseClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(7, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(361, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "BAKAS BİLİŞİM Baropodometric Kullanıcı Arayüzü";
            // 
            // lbl_hastakodu
            // 
            this.lbl_hastakodu.AutoSize = true;
            this.lbl_hastakodu.Location = new System.Drawing.Point(519, 14);
            this.lbl_hastakodu.Name = "lbl_hastakodu";
            this.lbl_hastakodu.Size = new System.Drawing.Size(0, 15);
            this.lbl_hastakodu.TabIndex = 7;
            this.lbl_hastakodu.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lbl_hastakodu_MouseClick);
            // 
            // lbl_solayaktanisitext
            // 
            this.lbl_solayaktanisitext.AutoSize = true;
            this.lbl_solayaktanisitext.Location = new System.Drawing.Point(142, 56);
            this.lbl_solayaktanisitext.Name = "lbl_solayaktanisitext";
            this.lbl_solayaktanisitext.Size = new System.Drawing.Size(0, 15);
            this.lbl_solayaktanisitext.TabIndex = 9;
            this.lbl_solayaktanisitext.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lbl_solayaktanisitext_MouseClick);
            // 
            // lbl_solayaktaniseviyesitext
            // 
            this.lbl_solayaktaniseviyesitext.AutoSize = true;
            this.lbl_solayaktaniseviyesitext.Location = new System.Drawing.Point(176, 81);
            this.lbl_solayaktaniseviyesitext.Name = "lbl_solayaktaniseviyesitext";
            this.lbl_solayaktaniseviyesitext.Size = new System.Drawing.Size(0, 15);
            this.lbl_solayaktaniseviyesitext.TabIndex = 11;
            this.lbl_solayaktaniseviyesitext.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lbl_solayaktaniseviyesitext_MouseClick);
            // 
            // lbl_sagayaktanisitext
            // 
            this.lbl_sagayaktanisitext.AutoSize = true;
            this.lbl_sagayaktanisitext.Location = new System.Drawing.Point(142, 157);
            this.lbl_sagayaktanisitext.Name = "lbl_sagayaktanisitext";
            this.lbl_sagayaktanisitext.Size = new System.Drawing.Size(0, 15);
            this.lbl_sagayaktanisitext.TabIndex = 13;
            this.lbl_sagayaktanisitext.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lbl_sagayaktanisitext_MouseClick);
            // 
            // lbl_sagayaktaniseviyesitext
            // 
            this.lbl_sagayaktaniseviyesitext.AutoSize = true;
            this.lbl_sagayaktaniseviyesitext.Location = new System.Drawing.Point(180, 182);
            this.lbl_sagayaktaniseviyesitext.Name = "lbl_sagayaktaniseviyesitext";
            this.lbl_sagayaktaniseviyesitext.Size = new System.Drawing.Size(0, 15);
            this.lbl_sagayaktaniseviyesitext.TabIndex = 15;
            this.lbl_sagayaktaniseviyesitext.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lbl_sagayaktaniseviyesitext_MouseClick);
            // 
            // lbl_scaletext
            // 
            this.lbl_scaletext.AutoSize = true;
            this.lbl_scaletext.Location = new System.Drawing.Point(655, 14);
            this.lbl_scaletext.Name = "lbl_scaletext";
            this.lbl_scaletext.Size = new System.Drawing.Size(0, 15);
            this.lbl_scaletext.TabIndex = 17;
            this.lbl_scaletext.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lbl_scaletext_MouseClick);
            // 
            // lbl_filtrelertext
            // 
            this.lbl_filtrelertext.AutoSize = true;
            this.lbl_filtrelertext.Location = new System.Drawing.Point(826, 14);
            this.lbl_filtrelertext.Name = "lbl_filtrelertext";
            this.lbl_filtrelertext.Size = new System.Drawing.Size(0, 15);
            this.lbl_filtrelertext.TabIndex = 19;
            this.lbl_filtrelertext.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lbl_filtrelertext_MouseClick);
            // 
            // pb1_border
            // 
            this.pb1_border.Controls.Add(this.pictureBox1);
            this.pb1_border.Location = new System.Drawing.Point(386, 115);
            this.pb1_border.Name = "pb1_border";
            this.pb1_border.Size = new System.Drawing.Size(266, 522);
            this.pb1_border.TabIndex = 21;
            this.pb1_border.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pb1_border_MouseClick);
            // 
            // pb2_border
            // 
            this.pb2_border.BackColor = System.Drawing.SystemColors.Control;
            this.pb2_border.Controls.Add(this.pictureBox2);
            this.pb2_border.Location = new System.Drawing.Point(653, 115);
            this.pb2_border.Name = "pb2_border";
            this.pb2_border.Size = new System.Drawing.Size(266, 522);
            this.pb2_border.TabIndex = 22;
            this.pb2_border.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pb2_border_MouseClick);
            // 
            // lbl_sol
            // 
            this.lbl_sol.AutoSize = true;
            this.lbl_sol.Location = new System.Drawing.Point(448, 97);
            this.lbl_sol.Name = "lbl_sol";
            this.lbl_sol.Size = new System.Drawing.Size(0, 15);
            this.lbl_sol.TabIndex = 23;
            // 
            // lbl_sag
            // 
            this.lbl_sag.AutoSize = true;
            this.lbl_sag.Location = new System.Drawing.Point(719, 98);
            this.lbl_sag.Name = "lbl_sag";
            this.lbl_sag.Size = new System.Drawing.Size(0, 15);
            this.lbl_sag.TabIndex = 24;
            // 
            // progressBar
            // 
            this.progressBar.Enabled = false;
            this.progressBar.Location = new System.Drawing.Point(391, 643);
            this.progressBar.MarqueeAnimationSpeed = 0;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(523, 23);
            this.progressBar.Step = 1;
            this.progressBar.TabIndex = 1;
            this.progressBar.Visible = false;
            // 
            // lbl_soltanimseviyesi
            // 
            this.lbl_soltanimseviyesi.AutoSize = true;
            this.lbl_soltanimseviyesi.Location = new System.Drawing.Point(283, 129);
            this.lbl_soltanimseviyesi.Name = "lbl_soltanimseviyesi";
            this.lbl_soltanimseviyesi.Size = new System.Drawing.Size(0, 15);
            this.lbl_soltanimseviyesi.TabIndex = 26;
            // 
            // lbl_soltanimgrubu
            // 
            this.lbl_soltanimgrubu.AutoSize = true;
            this.lbl_soltanimgrubu.Location = new System.Drawing.Point(283, 103);
            this.lbl_soltanimgrubu.Name = "lbl_soltanimgrubu";
            this.lbl_soltanimgrubu.Size = new System.Drawing.Size(0, 15);
            this.lbl_soltanimgrubu.TabIndex = 25;
            // 
            // lbl_sagtanimseviyesi
            // 
            this.lbl_sagtanimseviyesi.AutoSize = true;
            this.lbl_sagtanimseviyesi.Location = new System.Drawing.Point(283, 229);
            this.lbl_sagtanimseviyesi.Name = "lbl_sagtanimseviyesi";
            this.lbl_sagtanimseviyesi.Size = new System.Drawing.Size(0, 15);
            this.lbl_sagtanimseviyesi.TabIndex = 28;
            // 
            // lbl_sagtanimgrubu
            // 
            this.lbl_sagtanimgrubu.AutoSize = true;
            this.lbl_sagtanimgrubu.Location = new System.Drawing.Point(283, 205);
            this.lbl_sagtanimgrubu.Name = "lbl_sagtanimgrubu";
            this.lbl_sagtanimgrubu.Size = new System.Drawing.Size(0, 15);
            this.lbl_sagtanimgrubu.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.471698F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(7, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(253, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "---------------------------------------------------------------------------------" +
    "-";
            // 
            // btn_english
            // 
            this.btn_english.BackgroundImage = global::Baropodometrik.Properties.Resources._55ea65fdf018fbb8f87d5486;
            this.btn_english.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_english.Location = new System.Drawing.Point(844, 34);
            this.btn_english.Name = "btn_english";
            this.btn_english.Size = new System.Drawing.Size(75, 47);
            this.btn_english.TabIndex = 31;
            this.btn_english.UseVisualStyleBackColor = true;
            this.btn_english.Click += new System.EventHandler(this.btn_english_Click);
            // 
            // btn_turkish
            // 
            this.btn_turkish.BackgroundImage = global::Baropodometrik.Properties.Resources._2000px_Flag_of_Turkey_svg;
            this.btn_turkish.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_turkish.Location = new System.Drawing.Point(757, 34);
            this.btn_turkish.Name = "btn_turkish";
            this.btn_turkish.Size = new System.Drawing.Size(75, 47);
            this.btn_turkish.TabIndex = 30;
            this.btn_turkish.UseVisualStyleBackColor = true;
            this.btn_turkish.Click += new System.EventHandler(this.btn_turkish_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(5, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(256, 512);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseClick);
            this.pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
            this.pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseMove);
            this.pictureBox2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseUp);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(5, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(256, 512);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(270, 15);
            this.label5.TabIndex = 35;
            this.label5.Text = "SOL AYAK TANIMLANAN HASTA TANI SEVİYESİ:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(262, 15);
            this.label6.TabIndex = 34;
            this.label6.Text = "SOL AYAK TANIMLANAN HASTA TANI GRUBU:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(150, 15);
            this.label7.TabIndex = 33;
            this.label7.Text = "SOL AYAK TANI SEVİYESİ:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 15);
            this.label8.TabIndex = 32;
            this.label8.Text = "SOL AYAK TANISI:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 229);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(270, 15);
            this.label9.TabIndex = 39;
            this.label9.Text = "SAĞ AYAK TANIMLANAN HASTA TANI SEVİYESİ:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 205);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(262, 15);
            this.label10.TabIndex = 38;
            this.label10.Text = "SAĞ AYAK TANIMLANAN HASTA TANI GRUBU:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 182);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(150, 15);
            this.label11.TabIndex = 37;
            this.label11.Text = "SAĞ AYAK TANI SEVİYESİ:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 157);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(106, 15);
            this.label12.TabIndex = 36;
            this.label12.Text = "SAĞ AYAK TANISI:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(754, 14);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 15);
            this.label13.TabIndex = 42;
            this.label13.Text = "FİLTRELER:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(611, 14);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 15);
            this.label14.TabIndex = 41;
            this.label14.Text = "SCALE:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(431, 14);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(86, 15);
            this.label15.TabIndex = 40;
            this.label15.Text = "HASTA KODU:";
            // 
            // Anaekran
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 792);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btn_english);
            this.Controls.Add(this.btn_turkish);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_sagtanimseviyesi);
            this.Controls.Add(this.lbl_sagtanimgrubu);
            this.Controls.Add(this.lbl_soltanimseviyesi);
            this.Controls.Add(this.lbl_soltanimgrubu);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.lbl_sag);
            this.Controls.Add(this.lbl_sol);
            this.Controls.Add(this.pb2_border);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_filtrelertext);
            this.Controls.Add(this.lbl_scaletext);
            this.Controls.Add(this.lbl_sagayaktaniseviyesitext);
            this.Controls.Add(this.lbl_sagayaktanisitext);
            this.Controls.Add(this.lbl_solayaktaniseviyesitext);
            this.Controls.Add(this.lbl_solayaktanisitext);
            this.Controls.Add(this.lbl_hastakodu);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pb1_border);
            this.KeyPreview = true;
            this.Name = "Anaekran";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bakas Bilişim";
            this.Activated += new System.EventHandler(this.Anaekran_Activated);
            this.Deactivate += new System.EventHandler(this.Anaekran_Deactivate);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Anaekran_KeyDown);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Anaekran_MouseClick);
            this.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseScroll);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pb1_border.ResumeLayout(false);
            this.pb2_border.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_dosyaac;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_hastakodu;
        private System.Windows.Forms.Label lbl_solayaktanisitext;
        private System.Windows.Forms.Label lbl_solayaktaniseviyesitext;
        private System.Windows.Forms.Label lbl_sagayaktanisitext;
        private System.Windows.Forms.Label lbl_sagayaktaniseviyesitext;
        private System.Windows.Forms.Label lbl_scaletext;
        private System.Windows.Forms.Label lbl_filtrelertext;
        private System.Windows.Forms.Button btn_lpg;
        private System.Windows.Forms.ComboBox cb_lpg;
        private System.Windows.Forms.ComboBox cb_lpm;
        private System.Windows.Forms.Button btn_lpm;
        private System.Windows.Forms.ComboBox cb_alan;
        private System.Windows.Forms.Button btn_dosyakaydet;
        private System.Windows.Forms.Button btn_mirror;
        private System.Windows.Forms.Button btn_gerial;
        private System.Windows.Forms.Panel pb1_border;
        private System.Windows.Forms.Panel pb2_border;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btn_yenidosyakaydet;
        private System.Windows.Forms.Label lbl_sol;
        private System.Windows.Forms.Label lbl_sag;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label lbl_alan;
        private System.Windows.Forms.Button btn_decisiontree;
        private System.Windows.Forms.ComboBox cb_decisiontree;
        private System.Windows.Forms.ComboBox cb_linearregression;
        private System.Windows.Forms.Button btn_linearregression;
        private System.Windows.Forms.ComboBox cb_randomforest;
        private System.Windows.Forms.Button btn_randomforest;
        private System.Windows.Forms.Label lbl_max;
        private System.Windows.Forms.Label lbl_min;
        private System.Windows.Forms.TextBox tb_min;
        private System.Windows.Forms.TextBox tb_max;
        private System.Windows.Forms.Label lbl_randomsiddeti;
        private System.Windows.Forms.Label lbl_soltanimseviyesi;
        private System.Windows.Forms.Label lbl_soltanimgrubu;
        private System.Windows.Forms.Label lbl_sagtanimseviyesi;
        private System.Windows.Forms.Label lbl_sagtanimgrubu;
        private System.Windows.Forms.Label lbl_despeckle;
        private System.Windows.Forms.CheckBox check_despeckle;
        private System.Windows.Forms.Button btn_dynamicrange;
        private System.Windows.Forms.Button btn_histogrameq;
        private System.Windows.Forms.Button btn_batch;
        private System.Windows.Forms.ComboBox cb_batch1;
        private System.Windows.Forms.ComboBox cb_batch2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_turkish;
        private System.Windows.Forms.Button btn_english;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
    }
}

